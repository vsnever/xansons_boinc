
BOINCDIR = ../..

ifeq ($(OpenMP),1)
    BOINCDIR = ../../../boinc
endif


EXECNAMEBASE = xansons_boinc
CXXFLAGS = -Wall -O3 -std=c++11 -I$(BOINCDIR)/lib -I$(BOINCDIR)/api
LDFLAGS = -L$(BOINCDIR)/mac_build/build/Deployment -L$(BOINCDIR)/lib -L$(BOINCDIR)/api -lboinc_api -lboinc -lpthread
objects = main.o tinyxml2.o block.o config.o ReadXML.o IO.o CalcFunctions.o

ifeq ($(OpenMP),1)
    CXX = g++
    CXXFLAGS += -static-libgcc -static-libstdc++
    LDFLAGS += -static-libgcc -static-libstdc++
else
    CXX = clang++
    CXXFLAGS += -DMAC_OS_X_VERSION_MAX_ALLOWED=1090 -DMAC_OS_X_VERSION_MIN_REQUIRED=1090
endif

EXECNAME = $(EXECNAMEBASE)_mac

ifeq ($(OpenMP),1)
    CXXFLAGS += -fopenmp -D UseOMP
    LDFLAGS += -fopenmp
    EXECNAME = $(EXECNAMEBASE)_OMP_mac
else
    ifeq ($(CUDA),1)
        NVCC = nvcc
        CXXFLAGS += -D UseCUDA
        NVCCFLAGS = -D_FORCE_INLINES -D UseCUDA -O3 -use_fast_math -I$(BOINCDIR)/lib -I$(BOINCDIR)/api
        GENCODE_FLAGS = -gencode arch=compute_20,code=sm_20 -gencode arch=compute_30,code=sm_30 -gencode arch=compute_35,code=sm_35 -gencode arch=compute_37,code=sm_37 -gencode arch=compute_50,code=sm_50 -gencode arch=compute_52,code=sm_52 -gencode arch=compute_52,code=compute_52
        LDFLAGS += -L/usr/local/cuda/lib -lcuda -lcudart_static
        EXECNAME = $(EXECNAMEBASE)_CUDA_mac
        objects += CalcFunctionsCUDA.o
    else 
        ifeq ($(OpenCL),1)
            CXXFLAGS += -D UseOCL
            LDFLAGS += -framework opencl -lboinc_opencl
            EXECNAME = $(EXECNAMEBASE)_OCL_mac
            objects += CalcFunctionsOCL.o
        endif
    endif
endif

ODIR = $(EXECNAME)_build
OBJ = $(patsubst %,$(ODIR)/%,$(objects))
DEPS_base = typedefs.h tinyxml2.h vect3d.h
DEPS_main = $(DEPS_base) config.h block.h

all: $(EXECNAME)

ifeq ($(CUDA),1)
$(EXECNAME): $(OBJ)
	$(NVCC) -o $@ $^ $(LDFLAGS)
else
$(EXECNAME): $(OBJ)
	$(CXX) -o $@ $^ $(LDFLAGS)
endif

$(ODIR)/tinyxml2.o: tinyxml2.cpp tinyxml2.h | $(ODIR)
	$(CXX) $(CXXFLAGS) -c tinyxml2.cpp -o $@

$(ODIR)/block.o: block.cpp $(DEPS_base) ReadXML_utils.h block.h | $(ODIR)
	$(CXX) $(CXXFLAGS) -c block.cpp -o $@
	
$(ODIR)/config.o: config.cpp $(DEPS_base) ReadXML_utils.h config.h | $(ODIR)
	$(CXX) $(CXXFLAGS) -c config.cpp -o $@

$(ODIR)/IO.o: IO.cpp $(DEPS_base) config.h | $(ODIR)
	$(CXX) $(CXXFLAGS) -c IO.cpp -o $@

$(ODIR)/ReadXML.o: ReadXML.cpp $(DEPS_main) ReadXML_utils.h | $(ODIR)
	$(CXX) $(CXXFLAGS) -c ReadXML.cpp -o $@

$(ODIR)/CalcFunctions.o: CalcFunctions.cpp $(DEPS_main) | $(ODIR)
	$(CXX) $(CXXFLAGS) -c CalcFunctions.cpp -o $@

$(ODIR)/CalcFunctionsCUDA.o: CalcFunctionsCUDA.cu $(DEPS_main) | $(ODIR)
	$(NVCC) $(NVCCFLAGS) $(GENCODE_FLAGS) -c CalcFunctionsCUDA.cu -o $@

$(ODIR)/CalcFunctionsOCL.o: CalcFunctionsOCL.cpp $(DEPS_main) | $(ODIR)
	$(CXX) $(CXXFLAGS) -c CalcFunctionsOCL.cpp -o $@
	
$(ODIR)/main.o: main.cpp $(DEPS_main) | $(ODIR)
	$(CXX) $(CXXFLAGS) -c main.cpp -o $@

$(ODIR):
	mkdir -p $(ODIR)

clean:
	rm -f $(ODIR)/*.o

cleanall:
	find . -type f -name '*.o' -exec rm {} +
