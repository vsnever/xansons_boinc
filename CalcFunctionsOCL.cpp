//Copyright (C) 2017, NRC "Kurchatov institute", http://www.nrcki.ru/e/engl.html, Moscow, Russia
//Author: Vladislav Neverov, vs-never@hotmail.com, neverov_vs@nrcki.ru
//
//This file is part of XaNSoNS BOINC.
//
//XaNSoNS BOINC is free software: you can redistribute it and / or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//XaNSoNS BOINC is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "typedefs.h"
#ifdef UseOCL
#include "config.h"
#include "block.h"
#include <chrono>
#define CL_USE_DEPRECATED_OPENCL_2_0_APIS
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

//Nvidia specific OpenCL extensions
#ifndef CL_DEVICE_COMPUTE_CAPABILITY_MAJOR_NV
/* cl_nv_device_attribute_query extension - no extension #define since it has no functions */
#define CL_DEVICE_COMPUTE_CAPABILITY_MAJOR_NV       0x4000
#define CL_DEVICE_COMPUTE_CAPABILITY_MINOR_NV       0x4001
#define CL_DEVICE_KERNEL_EXEC_TIMEOUT_NV            0x4005
#endif

//AMD specific OpenCL extensions
#ifndef CL_DEVICE_WAVEFRONT_WIDTH_AMD 
/* cl_amd_device_attribute_query extension - no extension #define since it has no functions */
#define CL_DEVICE_WAVEFRONT_WIDTH_AMD               0x4043
#endif

//Calculates rotational matrix (from CalcFunctions.cpp)
void calcRotMatrix(vect3d <double> * const RM0, vect3d <double> * const RM1, vect3d <double> * const RM2, const vect3d <double> euler, const unsigned int convention);

/**
	Organazies the computations of the histogram of interatomic distances with OpenCL

	@param OCLcontext      OpenCL context
	@param OCLdevice       OpenCL device
	@param OCLprogram      OpenCL program object for OCLcontext
	@param *rij_hist       Histogram of interatomic distances (device). The memory is allocated inside the function.
	@param ra              Atomic coordinate array (device)
	@param *NatomEl        Array containing the total number of atoms of each chemical element (host)
	@param *NatomEl_outer  Array containing the number of atoms of each chemical element including the atoms in the outer sphere (only if cfg.cutoff is True)
	@param *cfg            Parameters of simulation
*/
void calcHistOCL(const cl_context OCLcontext, const cl_device_id OCLdevice, const cl_program OCLprogram, cl_mem * const rij_hist, const cl_mem ra, \
	const unsigned int * const NatomEl, const unsigned int * const NatomEl_outer, const config * const cfg, ostringstream *boinc_msg);

/**
Adds the average density correction to the scattering intensity when the cut-off is enabled (cfg.cutoff == true)

	@param OCLcontext      OpenCL context
	@param OCLdevice       OpenCL device
	@param OCLprogram      OpenCL program object for OCLcontext
	@param dI              Scattering intensity array (device)
	@param *NatomEl        Array containing the total number of atoms of each chemical element (host)
	@param *cfg            Parameters of simulation
	@param dFF             X-ray atomic form-factor array for all chemical elements (device)
	@param SL              Array of neutron scattering lengths for all chemical elements
	@param dq              Scattering vector magnitude array (device)
	@param Ntot            Total number of atoms in the nanoparticle
*/
void AddCutoffOCL(const cl_context OCLcontext, const cl_device_id OCLdevice, const cl_program OCLprogram, const cl_mem dI, const unsigned int * const NatomEl, const config * const cfg, \
	const cl_mem dFF, const vector<double> SL, const cl_mem dq, const unsigned int Ntot, ostringstream *boinc_msg);

/**
	Organazies the computations of the scattering intensity (powder diffraction pattern) using the histogram of interatomic distances with OpenCL

	@param OCLcontext      OpenCL context
	@param OCLdevice       OpenCL device
	@param OCLprogram      OpenCL program object for OCLcontext
	@param **I             Scattering intensity array (host). The memory is allocated inside the function
	@param rij_hist        Histogram of interatomic distances (device).
	@param *NatomEl        Array containing the total number of atoms of each chemical element (host)
	@param *cfg            Parameters of simulation
	@param dFF             X-ray atomic form-factor arrays for all chemical elements (device)
	@param SL              Array of neutron scattering lengths for all chemical elements
	@param dq              Scattering vector magnitude array (device)
	@param Ntot            Total number of atoms in the nanoparticle
*/
void calcInt1DHistOCL(const cl_context OCLcontext, const cl_device_id OCLdevice, const cl_program OCLprogram, double ** const I, const cl_mem rij_hist, \
	const unsigned int * const NatomEl, const config * const cfg, const cl_mem dFF, const vector<double> SL, const cl_mem dq, const unsigned int Ntot, ostringstream *boinc_msg);

/**
	Depending on the computational scenario organazies the computations of the scattering intensity (powder diffraction pattern) or PDF using the histogram of interatomic distances with OpenCL

	@param OCLcontext      OpenCL context
	@param OCLdevice       OpenCL device
	@param OCLprogram      OpenCL program object for OCLcontext
	@param **I             Scattering intensity array (host). The memory is allocated inside the function.
	@param **PDF           PDF array (host). The memory is allocated inside the function.
	@param *cfg            Parameters of simulation
	@param *NatomEl        Array containing the total number of atoms of each chemical element (host)
	@param *NatomEl_outer  Array containing the number of atoms of each chemical element including the atoms in the outer sphere (only if cfg.cutoff is True)
	@param ra              Atomic coordinate array (device)
	@param dFF             X-ray atomic form-factors for all chemical elements (device)
	@param SL              Array of neutron scattering lengths for all chemical elements
	@param dq              Scattering vector magnitude array (device)
*/
void calcHistandPatternOCL(const cl_context OCLcontext, const cl_device_id OCLdevice, const cl_program OCLprogram, double ** const I, const config * const cfg, \
	const unsigned int * const NatomEl, const unsigned int * const NatomEl_outer, const cl_mem ra, const cl_mem dFF, const vector<double> SL, const cl_mem dq, ostringstream *boinc_msg);

/**
	Returns the theoretical peak performance of the OpenCL device. Return 0 if the vendor is unsupported.

	@param OCLdevice OpenCL device
	@param show      If True, show the device information on screen
*/
unsigned int GetGFLOPS(const cl_device_id OCLdevice, const bool show, ostringstream *boinc_msg);


/**
	Creates OpenCL context and builds OpenCL kernels

	@param *OCLcontext  OpenCL context
	@param *OCLprogram  OpenCL program object
	@param OCLdevice    OpenCL device
	@param *argv0       Absolute path to the executable (first argument in the argv[]). It is used to get the path to the .cl files
*/
int createContextOCL(cl_context * const OCLcontext, cl_program * const OCLprogram, const cl_device_id OCLdevice, const char * const argv0, ostringstream *boinc_msg);

/**
	Copies the atomic coordinates (ra), scattering vector magnitude (q) and the x-ray atomic form-factors (FF) to the device memory

	@param OCLcontext  OpenCL context
	@param OCLdevice   OpenCL device
	@param *q          Scattering vector magnitude (host)
	@param *cfg        Parameters of simulation
	@param *ra         Atomic coordinates (host)
	@param *dra        Atomic coordinates (device). The memory is allocated inside the function
	@param *dFF        X-ray atomic form-factors (device). The memory is allocated inside the function
	@param *dq         Scattering vector magnitude (device). The memory is allocated inside the function
	@param FF          X-ray atomic form-factors (host)
	@param Ntot        Total number of atoms in the nanoparticle
*/
void dataCopyOCL(const cl_context OCLcontext, const cl_device_id OCLdevice, const double * const q, const config *const cfg, const vector < vect3d <float> > *const ra, \
	cl_mem * const dra, cl_mem * const dFF, cl_mem * const dq, const vector <double*> FF);

/**
	Deletes the atomic coordinates (ra), scattering vector magnitude (dq), the x-ray atomic form-factors (dFF) from the device memory and frees the OpenCL context and program object

	@param OCLcontext  OpenCL context
	@param OCLprogram  OpenCL program object
	@param ra          Atomic coordinates (device)
	@param dFF         X-ray atomic form-factors (device)
	@param dq          Scattering vector magnitude (device)
	@param Nel         Total number of different chemical elements in the nanoparticle
*/
void delDataFromDeviceOCL(const cl_context OCLcontext, const cl_program OCLprogram, const cl_mem ra, const cl_mem dFF, const cl_mem dq, const unsigned int Nel);

const char* clGetErrorString(int errorCode) {
	switch (errorCode) {
	case 0: return "CL_SUCCESS";
	case -1: return "CL_DEVICE_NOT_FOUND";
	case -2: return "CL_DEVICE_NOT_AVAILABLE";
	case -3: return "CL_COMPILER_NOT_AVAILABLE";
	case -4: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
	case -5: return "CL_OUT_OF_RESOURCES";
	case -6: return "CL_OUT_OF_HOST_MEMORY";
	case -7: return "CL_PROFILING_INFO_NOT_AVAILABLE";
	case -8: return "CL_MEM_COPY_OVERLAP";
	case -9: return "CL_IMAGE_FORMAT_MISMATCH";
	case -10: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
	case -12: return "CL_MAP_FAILURE";
	case -13: return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
	case -14: return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
	case -15: return "CL_COMPILE_PROGRAM_FAILURE";
	case -16: return "CL_LINKER_NOT_AVAILABLE";
	case -17: return "CL_LINK_PROGRAM_FAILURE";
	case -18: return "CL_DEVICE_PARTITION_FAILED";
	case -19: return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";
	case -30: return "CL_INVALID_VALUE";
	case -31: return "CL_INVALID_DEVICE_TYPE";
	case -32: return "CL_INVALID_PLATFORM";
	case -33: return "CL_INVALID_DEVICE";
	case -34: return "CL_INVALID_CONTEXT";
	case -35: return "CL_INVALID_QUEUE_PROPERTIES";
	case -36: return "CL_INVALID_COMMAND_QUEUE";
	case -37: return "CL_INVALID_HOST_PTR";
	case -38: return "CL_INVALID_MEM_OBJECT";
	case -39: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
	case -40: return "CL_INVALID_IMAGE_SIZE";
	case -41: return "CL_INVALID_SAMPLER";
	case -42: return "CL_INVALID_BINARY";
	case -43: return "CL_INVALID_BUILD_OPTIONS";
	case -44: return "CL_INVALID_PROGRAM";
	case -45: return "CL_INVALID_PROGRAM_EXECUTABLE";
	case -46: return "CL_INVALID_KERNEL_NAME";
	case -47: return "CL_INVALID_KERNEL_DEFINITION";
	case -48: return "CL_INVALID_KERNEL";
	case -49: return "CL_INVALID_ARG_INDEX";
	case -50: return "CL_INVALID_ARG_VALUE";
	case -51: return "CL_INVALID_ARG_SIZE";
	case -52: return "CL_INVALID_KERNEL_ARGS";
	case -53: return "CL_INVALID_WORK_DIMENSION";
	case -54: return "CL_INVALID_WORK_GROUP_SIZE";
	case -55: return "CL_INVALID_WORK_ITEM_SIZE";
	case -56: return "CL_INVALID_GLOBAL_OFFSET";
	case -57: return "CL_INVALID_EVENT_WAIT_LIST";
	case -58: return "CL_INVALID_EVENT";
	case -59: return "CL_INVALID_OPERATION";
	case -60: return "CL_INVALID_GL_OBJECT";
	case -61: return "CL_INVALID_BUFFER_SIZE";
	case -62: return "CL_INVALID_MIP_LEVEL";
	case -63: return "CL_INVALID_GLOBAL_WORK_SIZE";
	case -64: return "CL_INVALID_PROPERTY";
	case -65: return "CL_INVALID_IMAGE_DESCRIPTOR";
	case -66: return "CL_INVALID_COMPILER_OPTIONS";
	case -67: return "CL_INVALID_LINKER_OPTIONS";
	case -68: return "CL_INVALID_DEVICE_PARTITION_COUNT";
	case -69: return "CL_INVALID_PIPE_SIZE";
	case -70: return "CL_INVALID_DEVICE_QUEUE";
	case -71: return "CL_INVALID_SPEC_ID";
	case -72: return "CL_MAX_SIZE_RESTRICTION_EXCEEDED";
	case -1002: return "CL_INVALID_D3D10_DEVICE_KHR";
	case -1003: return "CL_INVALID_D3D10_RESOURCE_KHR";
	case -1004: return "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR";
	case -1005: return "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR";
	case -1006: return "CL_INVALID_D3D11_DEVICE_KHR";
	case -1007: return "CL_INVALID_D3D11_RESOURCE_KHR";
	case -1008: return "CL_D3D11_RESOURCE_ALREADY_ACQUIRED_KHR";
	case -1009: return "CL_D3D11_RESOURCE_NOT_ACQUIRED_KHR";
	case -1010: return "CL_INVALID_DX9_MEDIA_ADAPTER_KHR";
	case -1011: return "CL_INVALID_DX9_MEDIA_SURFACE_KHR";
	case -1012: return "CL_DX9_MEDIA_SURFACE_ALREADY_ACQUIRED_KHR";
	case -1013: return "CL_DX9_MEDIA_SURFACE_NOT_ACQUIRED_KHR";
	case -1093: return "CL_INVALID_EGL_OBJECT_KHR";
	case -1092: return "CL_EGL_RESOURCE_NOT_ACQUIRED_KHR";
	case -1001: return "CL_PLATFORM_NOT_FOUND_KHR";
	case -1057: return "CL_DEVICE_PARTITION_FAILED_EXT";
	case -1058: return "CL_INVALID_PARTITION_COUNT_EXT";
	case -1059: return "CL_INVALID_PARTITION_NAME_EXT";
	case -1094: return "CL_INVALID_ACCELERATOR_INTEL";
	case -1095: return "CL_INVALID_ACCELERATOR_TYPE_INTEL";
	case -1096: return "CL_INVALID_ACCELERATOR_DESCRIPTOR_INTEL";
	case -1097: return "CL_ACCELERATOR_TYPE_NOT_SUPPORTED_INTEL";
	case -1000: return "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR";
	case -1098: return "CL_INVALID_VA_API_MEDIA_ADAPTER_INTEL";
	case -1099: return "CL_INVALID_VA_API_MEDIA_SURFACE_INTEL";
	case -1100: return "CL_VA_API_MEDIA_SURFACE_ALREADY_ACQUIRED_INTEL";
	case -1101: return "CL_VA_API_MEDIA_SURFACE_NOT_ACQUIRED_INTEL";
	default: return "CL_UNKNOWN_ERROR";
	}
}


//Returns the theoretical peak performance of the OpenCL device. Return 0 if the vendor is unsupported.
unsigned int GetGFLOPS(const cl_device_id OCLdevice, const bool show = false, ostringstream *boinc_msg = NULL){
	cl_uint CU, GPUclock;	
	clGetDeviceInfo(OCLdevice, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &CU, NULL);
	clGetDeviceInfo(OCLdevice, CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(cl_uint), &GPUclock, NULL);
	size_t info_size;
	clGetDeviceInfo(OCLdevice, CL_DEVICE_NAME, 0, NULL, &info_size);
	char *name = new char[info_size / sizeof(char)];
	clGetDeviceInfo(OCLdevice, CL_DEVICE_NAME, info_size, name, NULL);
	if (show) {
		cout << "GPU: " << name << "\n";
		cout << "Number of compute units: " << CU << "\n";
		cout << "GPU clock rate: " << GPUclock << " MHz\n";
		if (boinc_msg) {
			*boinc_msg << "GPU: " << name << "\n";
			*boinc_msg << "Number of compute units: " << CU << "\n";
			*boinc_msg << "GPU clock rate: " << GPUclock << " MHz\n";
		}
	}
	delete[] name;
	clGetDeviceInfo(OCLdevice, CL_DEVICE_VENDOR, 0, NULL, &info_size);
	char *vendor = new char[info_size / sizeof(char)];
	clGetDeviceInfo(OCLdevice, CL_DEVICE_VENDOR, info_size, vendor, NULL);
	string vendor_str(vendor);
	delete[] vendor;
	transform(vendor_str.begin(), vendor_str.end(), vendor_str.begin(), ::tolower);
	unsigned int GFLOPS = 0;
	if (vendor_str.find("nvidia") != string::npos) {
		cl_uint CCmaj = 0;
		cl_uint CCmin = 0;
		clGetDeviceInfo(OCLdevice, CL_DEVICE_COMPUTE_CAPABILITY_MAJOR_NV, sizeof(cl_uint), &CCmaj, NULL);
		clGetDeviceInfo(OCLdevice, CL_DEVICE_COMPUTE_CAPABILITY_MINOR_NV, sizeof(cl_uint), &CCmin, NULL);
		const unsigned int cc = CCmaj * 10 + CCmin; //compute capability
		unsigned int ALUlanes = 64;	
		switch (cc){
		case 10:
		case 11:
		case 12:
		case 13:
			ALUlanes = 8;
			break;
		case 20:
			ALUlanes = 32;
			break;
		case 21:
			ALUlanes = 48;
			break;
		case 30:
		case 35:
		case 37:
			ALUlanes = 192;
			break;
		case 50:
		case 52:
			ALUlanes = 128;
			break;
		case 60:
			ALUlanes = 64;
			break;
		case 61:
		case 62:
			ALUlanes = 128;
			break;
		case 70:
		case 72:
		case 75:
			ALUlanes = 64;
			break;
		}
		GFLOPS = CU * ALUlanes * 2 * GPUclock / 1000;
	}
	else if (vendor_str.find("intel") != string::npos) {
		GFLOPS = CU * 16 * GPUclock / 1000;
	}
	else if ((vendor_str.find("amd") != string::npos) || (vendor_str.find("advanced") != string::npos)) {
		cl_uint WW = 0;
		const cl_int ret = clGetDeviceInfo(OCLdevice, CL_DEVICE_WAVEFRONT_WIDTH_AMD, sizeof(cl_uint), &WW, NULL);
		if ((ret != CL_SUCCESS) || (!WW)) WW = 64;//if CL_DEVICE_WAVEFRONT_WIDTH_AMD is not implemented
		GFLOPS = CU * WW * 2 * GPUclock / 1000;
	}
	else if (vendor_str.find("arm") != string::npos) {
		GFLOPS = CU * 2 * 16 * GPUclock / 1000; //16FP x 2 Vec4 (for Mali)
	}
	else {
		cout << "Error. Unsupported device vendor: " << vendor << endl;
		if (boinc_msg) *boinc_msg << "Error. Unsupported device vendor:" << vendor << "\n";
		return 0;
	}
	if (show) 	{
		cout << "Theoretical peak performance: " << GFLOPS << " GFLOPs\n" << endl;
		if (boinc_msg) *boinc_msg << "Theoretical peak performance: " << GFLOPS << " GFLOPs\n\n";
	}
	return GFLOPS;
}


void ShowDeviceOCL(const cl_device_id OCLdevice, ostringstream *boinc_msg){
	cout << "Selected OpenCL device:\n";
	*boinc_msg << "Selected OpenCL device:\n";
	GetGFLOPS(OCLdevice, true, boinc_msg);
}


//Creates OpenCL context and builds OpenCL kernels
int createContextOCL(cl_context * const OCLcontext, cl_program * const OCLprogram, const cl_device_id OCLdevice, const char * const argv0, ostringstream *boinc_msg){
	string path2kernels(argv0);
	size_t pos = path2kernels.rfind("/");
	if (pos == string::npos) pos = path2kernels.rfind("\\");
	boinc_resolve_filename_s("kernelsPDF.cl", path2kernels);
	char *source_str = NULL;
	size_t source_size;
	ifstream is(path2kernels, ifstream::binary);
	if (is) {
		is.seekg(0, is.end);
		int length = (int)is.tellg();
		is.seekg(0, is.beg);
		source_str = new char[length];
		is.read(source_str, length);
		source_size = sizeof(char)*length;
	}
	else {
		cout << "Failed to load OpenCL kernels from file.\n" << endl;
		*boinc_msg << "Failed to load OpenCL kernels from file.\n";
		exit(1);
	}
	size_t info_size = 0;
	clGetDeviceInfo(OCLdevice, CL_DEVICE_EXTENSIONS, 0, NULL, &info_size);
	char *info = new char[info_size / sizeof(char)];
	clGetDeviceInfo(OCLdevice, CL_DEVICE_EXTENSIONS, info_size, info, NULL);
	const string extensions(info);
	delete[] info;
	*OCLcontext = clCreateContext(NULL, 1, &OCLdevice, NULL, NULL, NULL);
	cl_int err;
	*OCLprogram = clCreateProgramWithSource(*OCLcontext, 1, (const char **)&source_str, &source_size, &err);
	string flags("-cl-fast-relaxed-math");
	if ((extensions.find("cl_khr_int64_base_atomics") == string::npos) && (extensions.find("cl_nv_") == string::npos)) flags.append(" -DCustomInt64atomics");
#ifdef ARMGPU
	flags.append(" -DARMGPU");
#endif
	err = clBuildProgram(*OCLprogram, 1, &OCLdevice, flags.c_str(), NULL, NULL);
	if (err) {
		size_t lengthErr;
		clGetProgramBuildInfo(*OCLprogram, OCLdevice, CL_PROGRAM_BUILD_LOG, 0, NULL, &lengthErr);
		char *buffer = new char[lengthErr];
		clGetProgramBuildInfo(*OCLprogram, OCLdevice, CL_PROGRAM_BUILD_LOG, lengthErr, buffer, NULL);
		cout << "--- Build log ---\n " << buffer << endl;
		*boinc_msg << "--- Build log ---\n " << buffer << "\n";
		delete[] buffer;
	}
	delete[] source_str;
	return 0;
}

//Copies the atomic coordinates (ra), scattering vector magnitude (q) and the x-ray atomic form-factors (FF) to the device memory
void dataCopyOCL(const cl_context OCLcontext, const cl_device_id OCLdevice, const double * const q, const config *const cfg, const vector < vect3d <float> > *const ra, \
				cl_mem * const dra, cl_mem * const dFF, cl_mem * const dq, const vector <double*> FF){
	size_t info_size;
	clGetDeviceInfo(OCLdevice, CL_DEVICE_VENDOR, 0, NULL, &info_size);
	char *vendor = new char[info_size / sizeof(char)];
	clGetDeviceInfo(OCLdevice, CL_DEVICE_VENDOR, info_size, vendor, NULL);
	string vendor_str(vendor);
	delete[] vendor;
	transform(vendor_str.begin(), vendor_str.end(), vendor_str.begin(), ::tolower);
	cl_bool UMflag = false;
	if ((vendor_str.find("intel") != string::npos) || (vendor_str.find("arm") != string::npos)) clGetDeviceInfo(OCLdevice, CL_DEVICE_HOST_UNIFIED_MEMORY, sizeof(cl_bool), &UMflag, NULL);//checking if GPU is integrated or not
	//copying the main data to the device memory
	unsigned int Nat = 0;
	for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++) Nat += (unsigned int)ra[iEl].size();
	if (UMflag) {//zero copy approach
		cl_command_queue queue = clCreateCommandQueue(OCLcontext, OCLdevice, 0, NULL);
		*dq = clCreateBuffer(OCLcontext, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, cfg->q.N * sizeof(cl_float), NULL, NULL);
		cl_float *qfloat = (cl_float *)clEnqueueMapBuffer(queue, *dq, true, CL_MAP_WRITE, 0, cfg->q.N * sizeof(cl_float), 0, NULL, NULL, NULL);
		for (unsigned int iq = 0; iq < cfg->q.N; iq++) qfloat[iq] = (cl_float)q[iq];
		clEnqueueUnmapMemObject(queue, *dq, (void *)qfloat, 0, NULL, NULL);
		if (cfg->source == xray) {
			*dFF = clCreateBuffer(OCLcontext, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, cfg->Nel * cfg->q.N * sizeof(cl_float), NULL, NULL);//device array for atomic form-factors
			cl_float *FFfloat = (cl_float *)clEnqueueMapBuffer(queue, *dFF, true, CL_MAP_WRITE, 0, cfg->Nel * cfg->q.N * sizeof(cl_float), 0, NULL, NULL, NULL);
			for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++){
				for (unsigned int iq = 0; iq < cfg->q.N; iq++) FFfloat[iEl * cfg->q.N + iq] = (cl_float)FF[iEl][iq];
			}
			clEnqueueUnmapMemObject(queue, *dFF, (void *)FFfloat, 0, NULL, NULL);
		}
		*dra = clCreateBuffer(OCLcontext, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, Nat * sizeof(cl_float4), NULL, NULL);
		cl_float4 *hra = (cl_float4 *)clEnqueueMapBuffer(queue, *dra, true, CL_MAP_WRITE, 0, Nat * sizeof(cl_float4), 0, NULL, NULL, NULL);
		unsigned int iAtom = 0;
		for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++){
			for (vector<vect3d <float> >::const_iterator ri = ra[iEl].begin(); ri != ra[iEl].end(); ri++, iAtom++){//converting atomic coordinates from vect3d <double> to float4
				hra[iAtom].s[0] = (cl_float)ri->x;
				hra[iAtom].s[1] = (cl_float)ri->y;
				hra[iAtom].s[2] = (cl_float)ri->z;
				hra[iAtom].s[3] = 0;
			}
		}
		clEnqueueUnmapMemObject(queue, *dra, (void *)hra, 0, NULL, NULL);
		clReleaseCommandQueue(queue);
		return;
	}
	//copying the data to device memory
	cl_float *qfloat = new cl_float[cfg->q.N];
	for (unsigned int iq = 0; iq < cfg->q.N; iq++) qfloat[iq] = (cl_float)q[iq];
	*dq = clCreateBuffer(OCLcontext, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, cfg->q.N * sizeof(cl_float), qfloat, NULL);
	delete[] qfloat;
	if (cfg->source == xray) {
		cl_float *FFfloat = new cl_float[cfg->Nel * cfg->q.N];
		for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++){
			for (unsigned int iq = 0; iq < cfg->q.N; iq++) FFfloat[iEl * cfg->q.N + iq] = (cl_float)FF[iEl][iq];
		}
		*dFF = clCreateBuffer(OCLcontext, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, cfg->Nel * cfg->q.N * sizeof(cl_float), FFfloat, NULL);
		delete[] FFfloat;
	}
	cl_float4 *hra = new cl_float4[Nat];
	unsigned int iAtom = 0;
	for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++){
		for (vector<vect3d <float> >::const_iterator ri = ra[iEl].begin(); ri != ra[iEl].end(); ri++, iAtom++){
			hra[iAtom].s[0] = (cl_float)ri->x;
			hra[iAtom].s[1] = (cl_float)ri->y;
			hra[iAtom].s[2] = (cl_float)ri->z;
			hra[iAtom].s[3] = 0;
		}
	}
	*dra = clCreateBuffer(OCLcontext, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, Nat * sizeof(cl_float4), hra, NULL);
	delete[] hra;
}

//Deletes the atomic coordinates (ra), scattering vector magnitude (dq), the x-ray atomic form-factors (dFF) from the device memory and frees the OpenCL context and program object
void delDataFromDeviceOCL(const cl_context OCLcontext, const cl_program OCLprogram, const cl_mem ra, const cl_mem dFF, const cl_mem dq, const unsigned int Nel){
	clReleaseProgram(OCLprogram);
	clReleaseContext(OCLcontext);
	clReleaseMemObject(ra);//deallocating device memory for the atomic coordinates array
	if (dq != NULL) clReleaseMemObject(dq);//deallocating memory for the scattering vector magnitude array
	if (dFF != NULL) clReleaseMemObject(dFF);//deallocating device memory for the atomic form-factors
}

//rganazies the computations of the histogram of interatomic distances with OpenCL
void calcHistOCL(const cl_context OCLcontext, const cl_device_id OCLdevice, const cl_program OCLprogram, cl_mem * const rij_hist, const cl_mem ra, \
					const unsigned int * const NatomEl, const unsigned int * const NatomEl_outer, const config * const cfg, ostringstream *boinc_msg){
	const unsigned int BlockSize = BlockSize1Dsmall, BlockSize2D = BlockSize2Dsmall; //size of the work-groups (256 by default, 16x16)
	const unsigned int NhistTotal = (cfg->Nel * (cfg->Nel + 1)) / 2 * cfg->Nhist;//NhistEl - number of partial (Element1<-->Element2) histograms
	cl_bool kernelExecTimeoutEnabled = true;	
	cl_device_type device_type;
	clGetDeviceInfo(OCLdevice, CL_DEVICE_TYPE, sizeof(cl_device_type), &device_type, NULL);//checking if device is dedicated accelerator
	if (device_type == CL_DEVICE_TYPE_ACCELERATOR) kernelExecTimeoutEnabled = false;
	size_t info_size;
	clGetDeviceInfo(OCLdevice, CL_DEVICE_VENDOR, 0, NULL, &info_size);
	char *vendor = new char[info_size / sizeof(char)];
	clGetDeviceInfo(OCLdevice, CL_DEVICE_VENDOR, info_size, vendor, NULL);
	string vendor_str(vendor);
	delete[] vendor;
	transform(vendor_str.begin(), vendor_str.end(), vendor_str.begin(), ::tolower);
	if (vendor_str.find("nvidia") != string::npos) {
		clGetDeviceInfo(OCLdevice, CL_DEVICE_KERNEL_EXEC_TIMEOUT_NV, sizeof(cl_bool), &kernelExecTimeoutEnabled, NULL);
	}
	const unsigned int GFLOPS = GetGFLOPS(OCLdevice); //theoretical peak GPU performance
	unsigned int GridSizeExecMax = 2048;
	if (kernelExecTimeoutEnabled){ //killswitch is enabled, so the time limit should not be exceeded
		const double tmax = 0.02; //maximum kernel time execution in seconds
		const double k = 1.e-6; // t = k * GridSizeExecMax^2 * BlockSize2D^2 / GFLOPS
		GridSizeExecMax = MIN((unsigned int)(sqrt(tmax * GFLOPS / k) / BlockSize2D), GridSizeExecMax);
	}
	else {
		*boinc_msg << "\nKernel execution timeout is disabled for this GPU\n";
	}
	*rij_hist = clCreateBuffer(OCLcontext, CL_MEM_READ_WRITE, NhistTotal * sizeof(cl_ulong), NULL, NULL);
	//creating kernels
	cl_kernel zeroHistKernel = clCreateKernel(OCLprogram, "zeroHistKernel", NULL); //reseting the histogram array
	clSetKernelArg(zeroHistKernel, 0, sizeof(cl_mem), (void *)rij_hist);
	clSetKernelArg(zeroHistKernel, 1, sizeof(cl_uint), (void *)&NhistTotal);
	cl_kernel calcHistKernel = clCreateKernel(OCLprogram, "calcHistKernel", NULL);
	clSetKernelArg(calcHistKernel, 0, sizeof(cl_mem), (void *)&ra);
	clSetKernelArg(calcHistKernel, 5, sizeof(cl_mem), (void *)rij_hist);
	const cl_float bin = (cl_float)cfg->hist_bin;
	clSetKernelArg(calcHistKernel, 7, sizeof(cl_float), (void *)&bin);
	const cl_uint cutoff = (cl_uint)cfg->cutoff;
	clSetKernelArg(calcHistKernel, 9, sizeof(cl_uint), (void *)&cutoff);
	const cl_float Rcut2 = (cl_float)SQR(cfg->Rcutoff);
	clSetKernelArg(calcHistKernel, 10, sizeof(cl_float), (void *)&Rcut2);
	const unsigned int GSzero = NhistTotal / BlockSize + BOOL(NhistTotal % BlockSize);//Size of the grid for zeroHistKernel (it must not be large than 65535)
	const size_t local_work_size_zero = BlockSize;
	const size_t global_work_size_zero = GSzero * local_work_size_zero;
	cl_command_queue queue = clCreateCommandQueue(OCLcontext, OCLdevice, 0, NULL);//Replace 0 with CL_QUEUE_PROFILING_ENABLE for profiling
	boinc_begin_critical_section();
	clEnqueueNDRangeKernel(queue, zeroHistKernel, 1, NULL, &global_work_size_zero, &local_work_size_zero, 0, NULL, NULL);
	boinc_end_critical_section();
	const size_t local_work_size[2] = { BlockSize2D, BlockSize2D};//2D thread block size
	unsigned int *indEl = new unsigned int[cfg->Nel];
	indEl[0] = 0;
	unsigned int Ntot_i = NatomEl[0];
	for (unsigned int iEl = 1; iEl < cfg->Nel; iEl++) {
		Ntot_i += NatomEl[iEl];
		(cfg->cutoff) ? indEl[iEl] = indEl[iEl - 1] + NatomEl_outer[iEl - 1] : indEl[iEl] = indEl[iEl - 1] + NatomEl[iEl - 1];		
	}
	unsigned int Ntot_j = indEl[cfg->Nel - 1];
	(cfg->cutoff) ? Ntot_j += NatomEl_outer[cfg->Nel - 1] : Ntot_j += NatomEl[cfg->Nel - 1];
	double Nop_total = double(Ntot_i) * (double(Ntot_j) - 0.5 * double(Ntot_i));
	double Nop_done = 0;
	unsigned int count = 0;
	//vector <double> kernel_exec_time;
	set <cl_int> EnqueueNDRangeErr;
	for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++) {
		unsigned int jElSt = iEl;
		if (cfg->cutoff) jElSt = 0;
		for (unsigned int jEl = jElSt; jEl < cfg->Nel; jEl++) {
			unsigned int jAtomST = 0;
			if ((cfg->cutoff) && (jEl < iEl)) jAtomST = NatomEl[jEl];
			unsigned int Nstart = 0;
			(jEl > iEl) ? Nstart = cfg->Nhist * (cfg->Nel * iEl - (iEl * (iEl + 1)) / 2 + jEl) : Nstart = cfg->Nhist * (cfg->Nel * jEl - (jEl * (jEl + 1)) / 2 + iEl);
			clSetKernelArg(calcHistKernel, 6, sizeof(cl_uint), (void *)&Nstart);
			for (unsigned int iAtom = 0; iAtom < NatomEl[iEl]; iAtom += BlockSize2D * GridSizeExecMax){
				const unsigned int i0 = indEl[iEl] + iAtom;
				clSetKernelArg(calcHistKernel, 1, sizeof(cl_uint), (void *)&i0);
				const unsigned int GridSizeExecY = MIN((NatomEl[iEl] - iAtom) / BlockSize2D + BOOL((NatomEl[iEl] - iAtom) % BlockSize2D), GridSizeExecMax);//Y-size of the grid on the current step
				const unsigned int iMax = MIN(BlockSize2D * GridSizeExecY, NatomEl[iEl] - iAtom);//index of the last i-th (row) atom				
				clSetKernelArg(calcHistKernel, 3, sizeof(cl_uint), (void *)&iMax);
				if (iEl == jEl) jAtomST = iAtom;//loop should exclude subdiagonal grids
				cl_ulong add = 2;
				clSetKernelArg(calcHistKernel, 11, sizeof(cl_ulong), (void *)&add);
				for (unsigned int jAtom = jAtomST; jAtom < NatomEl[jEl]; jAtom += BlockSize2D * GridSizeExecMax){
					const unsigned int j0 = indEl[jEl] + jAtom;
					clSetKernelArg(calcHistKernel, 2, sizeof(cl_uint), (void *)&j0);
					const unsigned int GridSizeExecX = MIN((NatomEl[jEl] - jAtom) / BlockSize2D + BOOL((NatomEl[jEl] - jAtom) % BlockSize2D), GridSizeExecMax);//X-size of the grid on the current step
					const unsigned int jMax = MIN(BlockSize2D * GridSizeExecX, NatomEl[jEl] - jAtom);//index of the last j-th (column) atom
					clSetKernelArg(calcHistKernel, 4, sizeof(cl_uint), (void *)&jMax);
					const size_t global_work_size[2] = { BlockSize2D*GridSizeExecX, BlockSize2D*GridSizeExecY };
					cl_uint diag = 0;
					if ((iEl == jEl) && (iAtom == jAtom)) diag = 1;//checking if we are on the diagonal grid or not		
					clSetKernelArg(calcHistKernel, 8, sizeof(cl_uint), (void *)&diag);
					//cl_event event;
					boinc_begin_critical_section();
					cl_int err = clEnqueueNDRangeKernel(queue, calcHistKernel, 2, NULL, global_work_size, local_work_size, 0, NULL, NULL); //replace last NULL with &event for profiling
					if (kernelExecTimeoutEnabled) {						
						//clWaitForEvents(1, &event);
						clFlush(queue);
						if (cfg->wait4kernel) clFinish(queue);
						//cl_ulong time_start, time_end;
						//clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
						//clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);
						//const double time_ms = (time_end - time_start) * 1.e-6;
						//kernel_exec_time.push_back(time_ms);
						//cout << "calcHistKernel execution time is: " << time_ms << " ms\n" << endl;
					}
					EnqueueNDRangeErr.insert(err);
					boinc_end_critical_section();
					Nop_done += (1. - diag * 0.5) * (double)(iMax * jMax);
					if (!(count % 10)) boinc_fraction_done(0.01 + 0.98 * Nop_done / Nop_total);
					//if (!(count % 10)) printf("Fraction done: %.1f%%\n",1. + 98. * Nop_done / Nop_total);
					count++;
				}
				if (cfg->cutoff) {
					const cl_uint diag = 0;
					clSetKernelArg(calcHistKernel, 8, sizeof(cl_uint), (void *)&diag);
					add = 1;
					clSetKernelArg(calcHistKernel, 11, sizeof(cl_ulong), (void *)&add);					
					for (unsigned int jAtom = NatomEl[jEl]; jAtom < NatomEl_outer[jEl]; jAtom += BlockSize2D * GridSizeExecMax){
						const unsigned int j0 = indEl[jEl] + jAtom;
						clSetKernelArg(calcHistKernel, 2, sizeof(cl_uint), (void *)&j0);
						const unsigned int GridSizeExecX = MIN((NatomEl_outer[jEl] - jAtom) / BlockSize2D + BOOL((NatomEl_outer[jEl] - jAtom) % BlockSize2D), GridSizeExecMax);//X-size of the grid on the current step
						const unsigned int jMax = MIN(BlockSize2D * GridSizeExecX, NatomEl_outer[jEl] - jAtom);//index of the last j-th (column) atom
						clSetKernelArg(calcHistKernel, 4, sizeof(cl_uint), (void *)&jMax);
						const size_t global_work_size[2] = { BlockSize2D * GridSizeExecX, BlockSize2D * GridSizeExecY };
						//cl_event event;
						boinc_begin_critical_section();
						cl_int err = clEnqueueNDRangeKernel(queue, calcHistKernel, 2, NULL, global_work_size, local_work_size, 0, NULL, NULL); //replace last NULL with &event for profiling
						if (kernelExecTimeoutEnabled) {
							//clWaitForEvents(1, &event);
							clFlush(queue);
							if (cfg->wait4kernel) clFinish(queue);
							//cl_ulong time_start, time_end;
							//clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
							//clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);
							//const double time_ms = (time_end - time_start) * 1.e-6;
							//kernel_exec_time.push_back(time_ms);
							//cout << "calcHistKernel execution time is: " << time_ms << " ms\n" << endl;
						}
						EnqueueNDRangeErr.insert(err);
						boinc_end_critical_section();
						Nop_done += (double)(iMax * jMax);
						if (!(count % 10)) boinc_fraction_done(0.01 + 0.98 * Nop_done / Nop_total);
						//if (!(count % 10)) printf("Fraction done: %.1f%%\n",1. + 98. * Nop_done / Nop_total);
						count++;
					}
				}

			}
		}
	}
	bool iserr = false;
	for (set <cl_int>::iterator it = EnqueueNDRangeErr.begin(); it != EnqueueNDRangeErr.end(); it++) {
		if (*it) {
			iserr = true; 
			break;
		}
	}
	if (iserr) {
		*boinc_msg << "\nSome kernels in calcHistOCL() returned the following errors:\n";
		for (set <cl_int>::iterator it = EnqueueNDRangeErr.begin(); it != EnqueueNDRangeErr.end(); it++) {
			if (*it) *boinc_msg << clGetErrorString(*it) << "\n";
		}
		*boinc_msg << "\n";
	}
	//ofstream out;
	//out.open("calcHistOCL_kernel_exec_time.txt");
	//if (out.is_open()){
	//	for (vector <double>::iterator it = kernel_exec_time.begin(); it != kernel_exec_time.end(); it++)	out << *it << "\n";
	//	out.close();
	//}
	delete[] indEl;
	clFinish(queue);
	clReleaseCommandQueue(queue);
	clReleaseKernel(zeroHistKernel);
	clReleaseKernel(calcHistKernel);
}


void AddCutoffOCL(const cl_context OCLcontext, const cl_device_id OCLdevice, const cl_program OCLprogram, const cl_mem dI, const unsigned int * const NatomEl, const config * const cfg, \
					const cl_mem dFF, const vector<double> SL, const cl_mem dq, const unsigned int Ntot, ostringstream *boinc_msg) {
	const size_t local_work_size = BlockSize1Dsmall;
	const unsigned int GSadd = cfg->q.N / BlockSize1Dsmall + BOOL(cfg->q.N % BlockSize1Dsmall);
	const size_t global_work_size_add = GSadd * local_work_size;
	cl_int err = 0;
	if (cfg->source == xray) {		
		cl_mem dNatomEl = clCreateBuffer(OCLcontext, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, cfg->Nel * sizeof(cl_uint), (void *)NatomEl, NULL);
		cl_kernel AddCutoffKernel = clCreateKernel(OCLprogram, "AddCutoffKernelXray", NULL);
		clSetKernelArg(AddCutoffKernel, 0, sizeof(cl_mem), (void *)&dI);
		clSetKernelArg(AddCutoffKernel, 1, sizeof(cl_mem), (void *)&dq);
		clSetKernelArg(AddCutoffKernel, 2, sizeof(cl_mem), (void *)&dFF);
		clSetKernelArg(AddCutoffKernel, 3, sizeof(cl_mem), (void *)&dNatomEl);
		clSetKernelArg(AddCutoffKernel, 4, sizeof(cl_uint), (void *)&cfg->Nel);
		clSetKernelArg(AddCutoffKernel, 5, sizeof(cl_uint), (void *)&Ntot);
		clSetKernelArg(AddCutoffKernel, 6, sizeof(cl_uint), (void *)&cfg->q.N);
		cl_float Rcut = (cl_float)cfg->Rcutoff;
		clSetKernelArg(AddCutoffKernel, 7, sizeof(cl_float), (void *)&Rcut);
		cl_float p0 = (cl_float)cfg->p0;
		clSetKernelArg(AddCutoffKernel, 8, sizeof(cl_float), (void *)&p0);
		cl_uint damping = (cl_uint)cfg->damping;
		clSetKernelArg(AddCutoffKernel, 9, sizeof(cl_uint), (void *)&damping);
		cl_command_queue queue = clCreateCommandQueue(OCLcontext, OCLdevice, 0, NULL);
		boinc_begin_critical_section();
		err = clEnqueueNDRangeKernel(queue, AddCutoffKernel, 1, NULL, &global_work_size_add, &local_work_size, 0, NULL, NULL);		
		boinc_end_critical_section();
		clFinish(queue);
		clReleaseCommandQueue(queue);
		clReleaseMemObject(dNatomEl);
		clReleaseKernel(AddCutoffKernel);
	}
	else {
		cl_float SLav = 0;
		for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++) SLav += (cl_float)(SL[iEl]) * NatomEl[iEl];
		SLav /= Ntot;
		cl_kernel AddCutoffKernel = clCreateKernel(OCLprogram, "AddCutoffKernelNeutron", NULL);
		clSetKernelArg(AddCutoffKernel, 0, sizeof(cl_mem), (void *)&dI);
		clSetKernelArg(AddCutoffKernel, 1, sizeof(cl_mem), (void *)&dq);
		clSetKernelArg(AddCutoffKernel, 2, sizeof(cl_float), (void *)&SLav);
		clSetKernelArg(AddCutoffKernel, 3, sizeof(cl_uint), (void *)&Ntot);
		clSetKernelArg(AddCutoffKernel, 4, sizeof(cl_uint), (void *)&cfg->q.N);
		cl_float Rcut = (cl_float)cfg->Rcutoff;
		clSetKernelArg(AddCutoffKernel, 5, sizeof(cl_float), (void *)&Rcut);
		cl_float p0 = (cl_float)cfg->p0;
		clSetKernelArg(AddCutoffKernel, 6, sizeof(cl_float), (void *)&p0);
		cl_uint damping = (cl_uint)cfg->damping;
		clSetKernelArg(AddCutoffKernel, 7, sizeof(cl_uint), (void *)&damping);
		cl_command_queue queue = clCreateCommandQueue(OCLcontext, OCLdevice, 0, NULL);
		boinc_begin_critical_section();
		err = clEnqueueNDRangeKernel(queue, AddCutoffKernel, 1, NULL, &global_work_size_add, &local_work_size, 0, NULL, NULL);
		boinc_end_critical_section();
		clFinish(queue);
		clReleaseCommandQueue(queue);
		clReleaseKernel(AddCutoffKernel);
	}
	if (err) *boinc_msg << "\nThe kernel in AddCutoffOCL() returned the error: \n" << clGetErrorString(err) << "\n\n";
}


//Organazies the computations of the scattering intensity (powder diffraction pattern) using the histogram of interatomic distances with OpenCL
void calcInt1DHistOCL(const cl_context OCLcontext, const cl_device_id OCLdevice, const cl_program OCLprogram, double ** const I, const cl_mem rij_hist, \
						const unsigned int * const NatomEl, const config * const cfg, const cl_mem dFF, const vector<double> SL, const cl_mem dq, const unsigned int Ntot, ostringstream *boinc_msg){
	size_t info_size;
	cl_device_type device_type;	
	clGetDeviceInfo(OCLdevice, CL_DEVICE_TYPE, sizeof(cl_device_type), &device_type, NULL);//checking if device is dedicated accelerator
	cl_bool kernelExecTimeoutEnabled = true;
	if (device_type == CL_DEVICE_TYPE_ACCELERATOR) kernelExecTimeoutEnabled = false;
	clGetDeviceInfo(OCLdevice, CL_DEVICE_VENDOR, 0, NULL, &info_size);
	char *vendor = new char[info_size / sizeof(char)];
	clGetDeviceInfo(OCLdevice, CL_DEVICE_VENDOR, info_size, vendor, NULL);
	string vendor_str(vendor);
	delete[] vendor;
	transform(vendor_str.begin(), vendor_str.end(), vendor_str.begin(), ::tolower);
	if (vendor_str.find("nvidia") != string::npos) {
		clGetDeviceInfo(OCLdevice, CL_DEVICE_KERNEL_EXEC_TIMEOUT_NV, sizeof(cl_bool), &kernelExecTimeoutEnabled, NULL);
	}
	const unsigned int BlockSize = BlockSize1Dsmall;//setting the size of the thread blocks to 256 (default)
	const unsigned int GridSize = MIN(256, cfg->Nhist / BlockSize + BOOL(cfg->Nhist % BlockSize));
	const unsigned int GFLOPS = GetGFLOPS(OCLdevice); //theoretical peak GPU performance
	unsigned int MaxBinsPerBlock = cfg->Nhist / GridSize + BOOL(cfg->Nhist % GridSize);
	if (kernelExecTimeoutEnabled)	{//killswitch is enabled, so the time limit should not be exceeded
		const double tmax = 0.02; //maximum kernel time execution in seconds
		const double k = 1.5e-5; // t = k * Nq * MaxBinsPerBlock / GFLOPS
		MaxBinsPerBlock = MIN((unsigned int)(tmax * GFLOPS / (k * cfg->q.N)), MaxBinsPerBlock);
	}
	const unsigned int Isize = GridSize * cfg->q.N;//each wotk-group writes to it's own copy of scattering intensity array
	cl_bool UMflag = false;
	if ((vendor_str.find("intel") != string::npos) || (vendor_str.find("arm") != string::npos)) clGetDeviceInfo(OCLdevice, CL_DEVICE_HOST_UNIFIED_MEMORY, sizeof(cl_bool), &UMflag, NULL);//checking if GPU is integrated or not
	cl_mem dI = NULL;
	(UMflag) ? dI = clCreateBuffer(OCLcontext, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, Isize * sizeof(cl_float), NULL, NULL) : dI = clCreateBuffer(OCLcontext, CL_MEM_READ_WRITE, Isize * sizeof(cl_float), NULL, NULL);
	//creating kernels
	cl_kernel zero1DFloatArrayKernel = clCreateKernel(OCLprogram, "zero1DFloatArrayKernel", NULL);
	clSetKernelArg(zero1DFloatArrayKernel, 0, sizeof(cl_mem), (void *)&dI);
	clSetKernelArg(zero1DFloatArrayKernel, 1, sizeof(cl_uint), (void *)&Isize);
	const unsigned int GSzero = Isize / BlockSize + BOOL(Isize % BlockSize);//grid size for zero1DFloatArrayKernel
	const size_t local_work_size = BlockSize;
	const size_t global_work_size_zero = GSzero * local_work_size;
	cl_command_queue queue = clCreateCommandQueue(OCLcontext, OCLdevice, 0, NULL);//Replace 0 with CL_QUEUE_PROFILING_ENABLE for profiling
	boinc_begin_critical_section();
	clEnqueueNDRangeKernel(queue, zero1DFloatArrayKernel, 1, NULL, &global_work_size_zero, &local_work_size, 0, NULL, NULL);//reseting intensity array
	
	boinc_end_critical_section();
	const unsigned int GSadd = cfg->q.N / BlockSize + BOOL(cfg->q.N % BlockSize);//grid size for addIKernelXray/addIKernelNeutron
	const size_t global_work_size_add = GSadd * local_work_size;
	set <cl_int> EnqueueNDRangeErr;
	if (cfg->source == xray) {
		cl_kernel addIKernel = clCreateKernel(OCLprogram, "addIKernelXray", NULL); //add contribution form diagonal (i==j) elements in Debye sum
		clSetKernelArg(addIKernel, 0, sizeof(cl_mem), (void *)&dI);
		clSetKernelArg(addIKernel, 1, sizeof(cl_uint), (void *)&cfg->q.N);
		clSetKernelArg(addIKernel, 2, sizeof(cl_mem), (void *)&dFF);
		boinc_begin_critical_section();
		for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++) {
			clSetKernelArg(addIKernel, 3, sizeof(cl_uint), (void *)&iEl);
			clSetKernelArg(addIKernel, 4, sizeof(cl_uint), (void *)&NatomEl[iEl]);
			cl_int err = clEnqueueNDRangeKernel(queue, addIKernel, 1, NULL, &global_work_size_add, &local_work_size, 0, NULL, NULL);//add contribution form diagonal (i==j) elements in Debye sum
			EnqueueNDRangeErr.insert(err);
		}
		clFinish(queue);
		clReleaseKernel(addIKernel);
		boinc_end_critical_section();
	}
	else {
		cl_kernel addIKernel = clCreateKernel(OCLprogram, "addIKernelNeutron", NULL); //add contribution form diagonal (i==j) elements in Debye sum
		clSetKernelArg(addIKernel, 0, sizeof(cl_mem), (void *)&dI);
		clSetKernelArg(addIKernel, 1, sizeof(cl_uint), (void *)&cfg->q.N);
		boinc_begin_critical_section();
		for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++) {
			cl_float mult = float(SQR(SL[iEl]) * NatomEl[iEl]);
			clSetKernelArg(addIKernel, 2, sizeof(cl_float), (void *)&mult);
			cl_int err = clEnqueueNDRangeKernel(queue, addIKernel, 1, NULL, &global_work_size_add, &local_work_size, 0, NULL, NULL);//add contribution form diagonal (i==j) elements in Debye sum
			EnqueueNDRangeErr.insert(err);
		}
		clFinish(queue);
		clReleaseKernel(addIKernel);
		boinc_end_critical_section();
	}	
	cl_kernel calcIntHistKernel = clCreateKernel(OCLprogram, "calcIntHistKernel", NULL);
	clSetKernelArg(calcIntHistKernel, 0, sizeof(cl_uint), (void *)&cfg->source);
	clSetKernelArg(calcIntHistKernel, 1, sizeof(cl_mem), (void *)&dI);
	if (cfg->source == xray) {
		clSetKernelArg(calcIntHistKernel, 2, sizeof(cl_mem), (void *)&dFF);
		const cl_float zero = 0;
		clSetKernelArg(calcIntHistKernel, 5, sizeof(cl_float), (void *)&zero);
	}
	else {
		clSetKernelArg(calcIntHistKernel, 2, sizeof(cl_mem), NULL);
		const cl_uint zero = 0;
		clSetKernelArg(calcIntHistKernel, 3, sizeof(cl_uint), (void *)&zero);
		clSetKernelArg(calcIntHistKernel, 4, sizeof(cl_uint), (void *)&zero);
	}
	clSetKernelArg(calcIntHistKernel, 6, sizeof(cl_mem), (void *)&dq);
	clSetKernelArg(calcIntHistKernel, 7, sizeof(cl_uint), (void *)&cfg->q.N);
	clSetKernelArg(calcIntHistKernel, 8, sizeof(cl_mem), (void *)&rij_hist);
	clSetKernelArg(calcIntHistKernel, 11, sizeof(cl_uint), (void *)&cfg->Nhist);
	clSetKernelArg(calcIntHistKernel, 12, sizeof(cl_uint), (void *)&MaxBinsPerBlock);
	const cl_float hbin = (cl_float)cfg->hist_bin;
	clSetKernelArg(calcIntHistKernel, 13, sizeof(cl_float), (void *)&hbin);
	const cl_float Rcut = (cl_float)cfg->Rcutoff;
	clSetKernelArg(calcIntHistKernel, 14, sizeof(cl_float), (void *)&Rcut);
	const cl_uint damping = (cl_uint)cfg->damping;
	clSetKernelArg(calcIntHistKernel, 15, sizeof(cl_uint), (void *)&damping);
	unsigned int Nstart = 0;
	size_t global_work_size = GridSize * local_work_size;
	for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++) {
		if (cfg->source == xray) clSetKernelArg(calcIntHistKernel, 3, sizeof(cl_uint), (void *)&iEl);
		for (unsigned int jEl = iEl; jEl < cfg->Nel; jEl++, Nstart += cfg->Nhist){
			clSetKernelArg(calcIntHistKernel, 9, sizeof(cl_uint), (void *)&Nstart);
			if (cfg->source == xray) clSetKernelArg(calcIntHistKernel, 4, sizeof(cl_uint), (void *)&jEl);
			else {
				const cl_float SLij = (cl_float) (SL[iEl] * SL[jEl]);
				clSetKernelArg(calcIntHistKernel, 5, sizeof(cl_float), (void *)&SLij);
			}
			for (unsigned int iBin = 0; iBin < cfg->Nhist; iBin += GridSize * MaxBinsPerBlock) {//iterations to avoid killswitch triggering
				//cl_event event;				
				clSetKernelArg(calcIntHistKernel, 10, sizeof(cl_uint), (void *)&iBin);
				boinc_begin_critical_section();
				cl_int err = clEnqueueNDRangeKernel(queue, calcIntHistKernel, 1, NULL, &global_work_size, &local_work_size, 0, NULL, NULL);//replace last NULL with &event for profiling
				//clWaitForEvents(1, &event);
				clFlush(queue);
				if (cfg->wait4kernel) clFinish(queue);
				EnqueueNDRangeErr.insert(err);
				boinc_end_critical_section();
				//cl_ulong time_start, time_end;
				//double time;
				//clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
				//clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);
				//time = time_end - time_start;
				//cout << "calcIntHistKernel execution time is: " << time / 1000000.0 << " ms\n" << endl;
			}
		}
	}
	
	cl_kernel sumIKernel = clCreateKernel(OCLprogram, "sumIKernel", NULL); //summing intensity copies
	clSetKernelArg(sumIKernel, 0, sizeof(cl_mem), (void *)&dI);
	clSetKernelArg(sumIKernel, 1, sizeof(cl_uint), (void *)&cfg->q.N);
	clSetKernelArg(sumIKernel, 2, sizeof(cl_uint), (void *)&GridSize);
	boinc_begin_critical_section();
	cl_int err = clEnqueueNDRangeKernel(queue, sumIKernel, 1, NULL, &global_work_size_add, &local_work_size, 0, NULL, NULL);//summing intensity copies
	EnqueueNDRangeErr.insert(err);
	boinc_end_critical_section();
	if (cfg->cutoff) AddCutoffOCL(OCLcontext, OCLdevice, OCLprogram, dI, NatomEl, cfg, dFF, SL, dq, Ntot, boinc_msg);
	if (cfg->PolarFactor) {
		cl_kernel PolarFactor1DKernel = clCreateKernel(OCLprogram, "PolarFactor1DKernel", NULL);
		const cl_float lambdaf = float(cfg->lambda);
		clSetKernelArg(PolarFactor1DKernel, 0, sizeof(cl_mem), (void *)&dI);
		clSetKernelArg(PolarFactor1DKernel, 1, sizeof(cl_uint), (void *)&cfg->q.N);
		clSetKernelArg(PolarFactor1DKernel, 2, sizeof(cl_mem), (void *)&dq);
		clSetKernelArg(PolarFactor1DKernel, 3, sizeof(cl_float), (void *)&lambdaf);
		clEnqueueNDRangeKernel(queue, PolarFactor1DKernel, 1, NULL, &global_work_size_add, &local_work_size, 0, NULL, NULL);
		clFinish(queue);
		clReleaseKernel(PolarFactor1DKernel);
	}
	cl_float *hI = NULL; //host array for scattering intensity
	if (UMflag) hI = (cl_float *)clEnqueueMapBuffer(queue, dI, true, CL_MAP_READ, 0, cfg->q.N * sizeof(cl_float), 0, NULL, NULL, NULL);
	else {
		hI = new cl_float[cfg->q.N];
		clEnqueueReadBuffer(queue, dI, true, 0, cfg->q.N * sizeof(cl_float), (void *)hI, 0, NULL, NULL);
	}
	*I = new double[cfg->q.N];
	for (unsigned int iq = 0; iq < cfg->q.N; iq++) (*I)[iq] = double(hI[iq]) / Ntot;//normalizing	
	if (UMflag) clEnqueueUnmapMemObject(queue, dI, (void *)hI, 0, NULL, NULL);
	else delete[] hI;
	bool iserr = false;
	for (set <cl_int>::iterator it = EnqueueNDRangeErr.begin(); it != EnqueueNDRangeErr.end(); it++) {
		if (*it) {
			iserr = true;
			break;
		}
	}
	if (iserr) {
		*boinc_msg << "\nSome kernels in calcInt1DHistOCL() returned the following errors:\n";
		for (set <cl_int>::iterator it = EnqueueNDRangeErr.begin(); it != EnqueueNDRangeErr.end(); it++) {
			if (*it) *boinc_msg << clGetErrorString(*it) << "\n";
		}
		*boinc_msg << "\n";
	}
	clFinish(queue);
	clReleaseCommandQueue(queue);
	clReleaseKernel(zero1DFloatArrayKernel);
	clReleaseKernel(sumIKernel);
	clReleaseKernel(calcIntHistKernel);
	clReleaseMemObject(dI);
}

//Depending on the computational scenario organazies the computations of the scattering intensity(powder diffraction pattern) or PDF using the histogram of interatomic distances with OpenCL
void calcHistandPatternOCL(const cl_context OCLcontext, const cl_device_id OCLdevice, const cl_program OCLprogram, double ** const I, const config * const cfg, \
	const unsigned int * const NatomEl, const unsigned int * const NatomEl_outer, const cl_mem ra, const cl_mem dFF, const vector<double> SL, const cl_mem dq, ostringstream *boinc_msg) {
	chrono::steady_clock::time_point t1 = chrono::steady_clock::now();
	cl_mem rij_hist = NULL;//array for pair-distribution histogram (device only)
	calcHistOCL(OCLcontext, OCLdevice, OCLprogram, &rij_hist, ra, NatomEl, NatomEl_outer, cfg, boinc_msg);//calculating the histogram
	chrono::steady_clock::time_point t2 = chrono::steady_clock::now();
	cout << "Histogram calculation time: " << chrono::duration_cast< chrono::duration < float > >(t2 - t1).count() << " s" << endl;
	*boinc_msg << "Histogram calculation time: " << chrono::duration_cast<chrono::duration < float >>(t2 - t1).count() << " s\n";
	unsigned int Ntot = 0;
	for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++) Ntot += NatomEl[iEl];//calculating the total number of atoms	
	t1 = chrono::steady_clock::now();
	calcInt1DHistOCL(OCLcontext, OCLdevice, OCLprogram, I, rij_hist, NatomEl, cfg, dFF, SL, dq, Ntot, boinc_msg);//calculating the scattering intensity using the pair-distribution histogram
	t2 = chrono::steady_clock::now();
	cout << "1D pattern calculation time: " << chrono::duration_cast< chrono::duration < float > >(t2 - t1).count() << " s" << endl;
	*boinc_msg << "1D pattern calculation time: " << chrono::duration_cast< chrono::duration < float > >(t2 - t1).count() << " s\n";
	clReleaseMemObject(rij_hist);//deallocating memory for pair distribution histogram
}

#endif
