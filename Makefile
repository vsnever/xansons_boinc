
BOINCDIR = ../..

CXXFLAGS = -Wall -O3 -std=c++11 -I$(BOINCDIR)/lib -I$(BOINCDIR)/api -I$(BOINCDIR)
LDFLAGS = 

ifneq ($(CUDA),1)
    CXXFLAGS += -static-libgcc -static-libstdc++
    LDFLAGS += -static-libgcc -static-libstdc++
endif

objects = main.o tinyxml2.o block.o config.o ReadXML.o IO.o CalcFunctions.o

CXX = g++

EXECNAMEBASE = xansons_boinc

EXECNAME = $(EXECNAMEBASE)

ifeq ($(OpenMP),1)
    CXXFLAGS += -fopenmp -D UseOMP
    LDFLAGS += -fopenmp
    EXECNAME = $(EXECNAMEBASE)_OMP
else
    ifeq ($(CUDA),1)
        NVCC = nvcc
        CXXFLAGS += -D UseCUDA
        NVCCFLAGS = -D_FORCE_INLINES -D UseCUDA -O3 -use_fast_math -I$(BOINCDIR)/lib -I$(BOINCDIR)/api 
        GENCODE_FLAGS = -gencode arch=compute_13,code=sm_13 -gencode arch=compute_20,code=sm_20 -gencode arch=compute_30,code=sm_30 -gencode arch=compute_35,code=sm_35 -gencode arch=compute_37,code=sm_37 -gencode arch=compute_50,code=sm_50 -gencode arch=compute_52,code=sm_52 -gencode arch=compute_52,code=compute_52
        LDFLAGS += -lcuda -lcudart_static
        EXECNAME = $(EXECNAMEBASE)_CUDA
        objects += CalcFunctionsCUDA.o
    else 
        ifeq ($(OpenCL),1)
            CXXFLAGS += -D UseOCL
            LDFLAGS += -lOpenCL
            EXECNAME = $(EXECNAMEBASE)_OCL
            objects += CalcFunctionsOCL.o boinc_opencl.o
            ifeq ($(Mali),1)
                CXXFLAGS += -D ARMGPU
                LDFLAGS += -lmali
            endif
        endif
    endif
endif

ODIR = $(EXECNAME)_build
OBJ = $(patsubst %,$(ODIR)/%,$(objects))
DEPS_base = typedefs.h tinyxml2.h vect3d.h
DEPS_main = $(DEPS_base) config.h block.h

all: $(EXECNAME)

ifeq ($(CUDA),1)
$(EXECNAME): $(OBJ) libstdc++.a $(BOINCDIR)/api/libboinc_api.a $(BOINCDIR)/lib/libboinc.a
	$(NVCC) $(LDFLAGS) -o $@ $^ -lpthread 
else
$(EXECNAME): $(OBJ) libstdc++.a $(BOINCDIR)/api/libboinc_api.a $(BOINCDIR)/lib/libboinc.a
	$(CXX) $(LDFLAGS) -o $@ $^ -pthread 
endif

libstdc++.a:
	ln -s `g++ -print-file-name=libstdc++.a`

$(ODIR)/tinyxml2.o: tinyxml2.cpp tinyxml2.h | $(ODIR)
	$(CXX) $(CXXFLAGS) -c tinyxml2.cpp -o $@

$(ODIR)/block.o: block.cpp $(DEPS_base) ReadXML_utils.h block.h | $(ODIR)
	$(CXX) $(CXXFLAGS) -c block.cpp -o $@
	
$(ODIR)/config.o: config.cpp $(DEPS_base) ReadXML_utils.h config.h | $(ODIR)
	$(CXX) $(CXXFLAGS) -c config.cpp -o $@

$(ODIR)/IO.o: IO.cpp $(DEPS_base) config.h | $(ODIR)
	$(CXX) $(CXXFLAGS) -c IO.cpp -o $@

$(ODIR)/ReadXML.o: ReadXML.cpp $(DEPS_main) ReadXML_utils.h | $(ODIR)
	$(CXX) $(CXXFLAGS) -c ReadXML.cpp -o $@

$(ODIR)/CalcFunctions.o: CalcFunctions.cpp $(DEPS_main) | $(ODIR)
	$(CXX) $(CXXFLAGS) -c CalcFunctions.cpp -o $@

$(ODIR)/CalcFunctionsCUDA.o: CalcFunctionsCUDA.cu $(DEPS_main) | $(ODIR)
	$(NVCC) $(NVCCFLAGS) $(GENCODE_FLAGS) -c CalcFunctionsCUDA.cu -o $@

$(ODIR)/CalcFunctionsOCL.o: CalcFunctionsOCL.cpp $(DEPS_main) | $(ODIR)
	$(CXX) $(CXXFLAGS) -c CalcFunctionsOCL.cpp -o $@
	
$(ODIR)/main.o: main.cpp $(DEPS_main) | $(ODIR)
	$(CXX) $(CXXFLAGS) -c main.cpp -o $@
	
$(ODIR)/boinc_opencl.o: $(BOINCDIR)/api/boinc_opencl.cpp $(BOINCDIR)/api/boinc_opencl.h
	$(CXX) $(CXXFLAGS) -c $(BOINCDIR)/api/boinc_opencl.cpp -o $@

$(ODIR):
	mkdir -p $(ODIR)

clean:
	rm -f $(ODIR)/*.o

cleanall:
	find . -type f -name '*.o' -exec rm {} + libstdc++.a
