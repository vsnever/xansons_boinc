//Copyright (C) 2017, NRC "Kurchatov institute", http://www.nrcki.ru/e/engl.html, Moscow, Russia
//Author: Vladislav Neverov, vs-never@hotmail.com, neverov_vs@nrcki.ru
//
//This file is part of XaNSoNS BOINC.
//
//XaNSoNS BOINC is free software: you can redistribute it and / or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//XaNSoNS BOINC is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program. If not, see <http://www.gnu.org/licenses/>.

//functions, that read parameters from xml file are here
#include "config.h"
#include "block.h"
#include "ReadXML_utils.h"

int getN(const char * const filename);//IO.cpp

int checkFile(const char * const filename);//IO.cpp

int readNeuData(const char * const filename, map<string, unsigned int> * const ID, map<unsigned int, string> * const EN, vector<double> * const SL, const set <string> * const names, ostringstream *boinc_msg);//IO.cpp

int loadFF(const char *  const filename, map<string, unsigned int> * const ID, map<unsigned int, string> * const EN, vector <double *> * const FF, const double * const q, const unsigned int Nq, const set <string> * const names, ostringstream *boinc_msg);//IO.cpp

void calcRotMatrix(vect3d <double> * const RM0, vect3d <double> * const RM1, vect3d <double> * const RM2, const vect3d <double> euler, const unsigned int convention);//CalcFunctions.cpp


/**
Returns the number of XML elements with the specified name

@param *xNode  first XML element with the name 'name'
@param *name   Name of the XML element
*/
unsigned int getNumberofElements(tinyxml2::XMLElement *xNode, const char * const name);

/**
Parses the XML file with parameters of the simulation and the model sample
Returns negative value if error and 0 if OK.

@param *cfg       Parameters of simulation
@param **q        Scattering vector magnitude
@param **Block    Array of the structural blocks
@param *FileName  Path to XML file
@param *ID        Array of [chemical element name, index] pairs. The pairs are created in this function according to the file
@param *SL        Array of neutron scattering lengths for all chemical elements
@param *FF        X-ray atomic form-factor arrays for all chemical elements
*/
int ReadConfig(config * const cfg, double ** const q, block ** const Block, const char * const FileName, map<string, unsigned int> * const ID, map<unsigned int, string> * const EN, vector<double> * const SL, vector<double *> * const FF, ostringstream *boinc_msg);

//Returns the number of XML elements with the specified name
unsigned int getNumberofElements(tinyxml2::XMLElement *xNode, const char * const name){
	unsigned int count = 0;
	while (xNode){
		xNode = xNode->NextSiblingElement(name);
		count++;
	}
	return count;
}


//Parses the XML file with parameters of the simulation and the model sample
int ReadConfig(config * const cfg, double ** const q, block ** const Block, const char * const FileName, map<string, unsigned int> * const ID, map<unsigned int, string> * const EN, vector<double> * const SL, vector<double *> * const FF, ostringstream *boinc_msg){
	tinyxml2::XMLDocument doc;
	string resolved_name;
	int error = boinc_resolve_filename_s(FileName, resolved_name);
	if (error) {
		cout << "Error: can't resolve filename." << endl;
		*boinc_msg << "Error: can't resolve filename.\n";
		return -1;
	}
	const tinyxml2::XMLError res = doc.LoadFile(resolved_name.c_str());
	if (res != tinyxml2::XML_SUCCESS)  {
		cout << "Error: file " << FileName << " does not exist or contains errors." << endl;
		*boinc_msg << "Error: file " << FileName << " does not exist or contains errors.\n";
		return -1;
	}
	tinyxml2::XMLElement *xMainNode = doc.RootElement();
	cout << "\nParsing calculation parameters..." << endl;
	tinyxml2::XMLElement *calcNode = xMainNode->FirstChildElement("Calculation");
	if (!calcNode) {
		cout << "Parsing error: 'Calculation' element is missing." << endl;
		*boinc_msg << "Parsing error: 'Calculation' element is missing.\n";
		return -1;
	}
	error = cfg->ReadParameters(calcNode);
	*q = new double[cfg->q.N];
	const double deltaq = (cfg->q.max - cfg->q.min) / MAX(1,cfg->q.N-1);
	for (unsigned int iq = 0; iq<cfg->q.N; iq++) (*q)[iq] = cfg->q.min + iq*deltaq;	
	cfg->Nblocks = getNumberofElements(xMainNode->FirstChildElement("Block"), "Block");
	if (!cfg->Nblocks) {
		cout << "Parsing error: structural blocks are not specified." << endl;
		*boinc_msg << "Parsing error: structural blocks are not specified.\n";
		return error - 1;
	}
	*Block = new block[cfg->Nblocks];	
	tinyxml2::XMLElement *BlockNode = xMainNode->FirstChildElement("Block");
	for (unsigned int iB = 0; iB < cfg->Nblocks; iB++){
		cout << "\nParsing Block " << iB << "..." << endl;
		error += (*Block)[iB].ReadBlockParameters(BlockNode, &cfg->uncert);
		BlockNode = BlockNode->NextSiblingElement("Block");
	}
	cout << "\nAll blocks have been parsed.\n" << endl;
	set <string> names((*Block)[0].names);
	for (unsigned int iB = 1; iB < cfg->Nblocks; iB++) names.insert((*Block)[iB].names.begin(), (*Block)[iB].names.end());
	cfg->Nel = (unsigned int) names.size();
	if (cfg->source == xray) error += loadFF(cfg->FFfilename.c_str(), ID, EN, FF, *q, cfg->q.N, &names, boinc_msg);
	else error += readNeuData(cfg->FFfilename.c_str(), ID, EN, SL, &names, boinc_msg);
	return error;
}

