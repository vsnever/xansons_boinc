//Copyright (C) 2017, NRC "Kurchatov institute", http://www.nrcki.ru/e/engl.html, Moscow, Russia
//Author: Vladislav Neverov, vs-never@hotmail.com, neverov_vs@nrcki.ru
//
//This file is part of XaNSoNS BOINC.
//
//XaNSoNS BOINC is free software: you can redistribute it and / or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//XaNSoNS BOINC is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "config.h"
#include "block.h"
#include <random>
#include <chrono>
#ifdef UseOMP
#include <omp.h>
#endif

int printAtoms(const vector < vect3d<float> > * const ra, const unsigned int * const NatomEl, const string name, const map <unsigned int, string> EN, const unsigned int Ntot);//IO.cpp

/**
Calculates rotational matrix using the Euler angles in the case of intristic rotation in the user-defined rotational convention.
See http://en.wikipedia.org/wiki/Euler_angles#Relationship_to_other_representations for details.

@param *RM0        1st row of the rotational matrix
@param *RM1        2nd row of the rotational matrix
@param *RM2        3rd row of the rotational matrix
@param euler       Euler angles
@param convention  Rotational convention
*/
void calcRotMatrix(vect3d <double> * const RM0, vect3d <double> * const RM1, vect3d <double> * const RM2, const vect3d <double> euler, const unsigned int convention);

/**
Returns the geometric center of the atomic ensemble (nanoparticle)

@param *r    Atomic coordinates
@param Nel   Total number of different chemical elements in the nanoparticle
@param Ntot  Total number of atoms in the nanoparticle
*/
vect3d<float> GetCenter(const vector < vect3d<float> > * const r, const unsigned int Nel, const unsigned int Ntot);

/**
Returns the largest possible interatomic distance in the atomic ensemble (nanoperticle)

@param *r      Atomic coordinates
@param rCenter Geometric mean of the atomic ensemble
@param Nel     Total number of different chemical elements in the nanoparticle
@param Ntot    Total number of atoms in the nanoparticle
*/
double GetEnsembleSize(const vector < vect3d<float> > * const r, const vect3d <float> rCenter, const unsigned int Nel);

/**
Returns the average atomic density of the atomic ensemble (nanoperticle)

@param *r      Atomic coordinates
@param rCenter Geometric mean of the atomic ensemble
@param Rcutoff Cut-off radius. The average atomic density is calculated for the sphere with this radius placed in the rCenter
@param Nel     Total number of different chemical elements in the nanoparticle
*/
double GetAtomicDensity(const vector < vect3d<float> > * const r, const vect3d <float> rCenter, const double Rcutoff, const unsigned int Nel);

/**
Returns the average atomic density of the atomic ensemble if cfg->BoxEdge parameters are defined
*/
double GetAtomicDensityBox(const unsigned int N, const vect3d <double> *BoxEdge);

/**
Moves the atoms located inside the inner cut-off box in the beginning of r[iEl] vector.Removes from r[iEl] the atoms locted outside the outer cut-off box
@param *r              Atomic coordinates
@param *cfg            Parameters of simulation
@param **NatomEl       Array containing the total number of atoms of each chemical element (allocated in this fuction)
@param **NatomEl_outer Array containing the number of atoms of each chemical element including the atoms in the outer sphere (allocated in this fuction, only if cfg.cutoff is True)
*/
unsigned int cutoffBox(vector < vect3d <float> > * const r, const config * const cfg, unsigned int * const NatomEl, unsigned int * const NatomEl_outer);

/**
Moves the atoms located inside the inner cut-off sphere in the beginning of r[iEl] vector. Removes from r[iEl] the atoms locted outside the outer cut-off sphere

@param *r              Atomic coordinates
@param *cfg            Parameters of simulation
@param *NatomEl        Array containing the total number of atoms of each chemical element
@param *NatomEl_outer  Array containing the number of atoms of each chemical element including the atoms in the outer sphere (only if cfg.cutoff is True)
@param rCenter         Geometric mean of the atomic ensemble
*/
unsigned int cutoffSphere(vector < vect3d <float> > * const r, const config * const cfg, unsigned int * const NatomEl, unsigned int * const NatomEl_outer, const vect3d <float> rCenter);

/**
Calculates the total atomic ensemble and prints it in the .xyz file if cfg.PrintAtoms is true. 
Returns the total number of atoms in the atomic ensemble (nanoparticle).

@param *cfg            Parameters of simulation
@param *Block          Array of the structural blocks
@param **ra            Atomic coordinates (allocated in this fuction)
@param **NatomEl       Array containing the total number of atoms of each chemical element (allocated in this fuction)
@param **NatomEl_outer Array containing the number of atoms of each chemical element including the atoms in the outer sphere (allocated in this fuction, only if cfg.cutoff is True)
@param ID              Array of [chemical element name, index] pairs. The pairs are created in this function according to the file
*/
unsigned int CalcAndPrintAtoms(config * const cfg, block * const Block, vector < vect3d <float> > ** const ra, unsigned int ** const NatomEl, unsigned int ** const NatomEl_outer, const map <string, unsigned int> ID, const map <unsigned int, string> EN, ostringstream *boinc_msg);

#if !defined(UseOCL) && !defined(UseCUDA)

/**
Computes the polarization factor and multiplies scattering intensity by this factor

@param *I      Scattering intensity array
@param *q      Scattering vector magnitude array
@param *cfg    Parameters of simulation
*/
void PolarFactor1D(double * const I, const double * const q, const config * const cfg);

/**
Adds the average density correction to the scattering intensity when the cut-off is enabled (cfg.cutoff == true)

@param *I             Scattering intensity array.
@param *cfg           Parameters of simulation
@param *NatomEl       Array containing the total number of atoms of each chemical element
@param FF             X-ray atomic form-factor arrays for all chemical elements
@param SL             Array of neutron scattering lengths for all chemical elements
@param *q             Scattering vector magnitude array
@param Ntot           Total number of atoms in the nanoparticle
*/
void AddCutoff(double * const I, const config * const cfg, const unsigned int * const NatomEl, const double * const q, const vector <double*> FF, const vector<double> SL, const unsigned int Ntot);

/**
Computes the contribution to the histogram of interatomic distances from the atoms of the chemical elements with indeces iEl and jEl

@param *rij_hist      Histogram of interatomic distances.
@param *cfg           Parameters of simulation
@param *ra            Atomic coordinate array
@param NatomEli       The total number of atoms of the chemical element iEl
@param NatomElj       The total number of atoms of the chemical element jEl
@param iEl            i-th chemical element index
@param jEl            j-th chemical element index
@param id             Unique thread id (MPI, OpenMP, MPI + OpenMP)
@param NumOMPthreads  Number of OpenMP threads
*/
void HistCore(unsigned long long int * const __restrict rij_hist, const config * const __restrict cfg, const vector < vect3d <float> > * const __restrict ra, const unsigned int NatomEli, const unsigned int NatomElj, const unsigned int iEl, const unsigned int jEl, const unsigned int id, const int NumOMPthreads, double *Nop_done, double Nop_total, const double frac_hist);

/**
Computes the contribution to the histogram of interatomic distances from the atoms of the chemical elements with indeces iEl and jEl (if cfg->cutoff == true)

@param *rij_hist      Histogram of interatomic distances.
@param *cfg           Parameters of simulation
@param *ra            Atomic coordinate array
@param NatomEli       The total number of atoms of the chemical element iEl
@param NatomElj       The total number of atoms of the chemical element jEl
@param NatomElj_out   The total number of atoms of the chemical element jEl including the atoms in the outer sphere
@param iEl            i-th chemical element index
@param jEl            j-th chemical element index
@param id             Unique thread id (MPI, OpenMP, MPI + OpenMP)
@param NumOMPthreads  Number of OpenMP threads
*/
void HistCoreCutOff(unsigned long long int * const __restrict rij_hist, const config * const __restrict cfg, const vector < vect3d <float> > * const __restrict ra, const unsigned int NatomEli, const unsigned int NatomElj, const unsigned int NatomElj_out, const unsigned int iEl, const unsigned int jEl, const unsigned int id, const int NumOMPthreads, double *Nop_done, double Nop_total, const double frac_hist);

/**
Computes the histogram of interatomic distances 

@param **rij_hist     Histogram of interatomic distances. The memory is allocated inside the function
@param *cfg           Parameters of simulation
@param *ra            Atomic coordinate array
@param *NatomEl       Array containing the total number of atoms of each chemical element
@param *NatomEl_outer Array containing the number of atoms of each chemical element including the atoms in the outer sphere (only if cfg.cutoff is True)
@param NumOMPthreads  Number of OpenMP threads
*/
double calcHist(unsigned long long int ** const rij_hist, const config *const cfg, const vector < vect3d <float> > * const ra, const unsigned int * const NatomEl, const unsigned int * const NatomEl_outer, const int NumOMPthreads);

/**
Computes the partial scattering intensity for a pair of elements using the histogram of interatomic distances

@param *I             Scattering intensity array
@param *rij_hist      Part fo the histogram of interatomic distances corresponding to a pair of elements
@param *cfg           Parameters of simulation
@param Nhist          Length of the part of rij_hist processed by current MPI process (cfg->Nhist if UseMPI is not defined)
@param iStart         Index of the first histogram bin processed by current MPI process (0 if UseMPI is not defined)
@param NumOMPthreads  Number of OpenMP threads
*/
void Int1DHistCore(double * const __restrict I, const unsigned long long int * const __restrict rij_hist, const config * const __restrict cfg, const double * const __restrict q, const unsigned int Nhist, const unsigned int iStart, const int NumOMPthreads, const unsigned int Nstart, const double frac);

/**
Computes the scattering intensity (powder diffraction pattern) using the histogram of interatomic distances

@param **I            Scattering intensity array. The memory is allocated inside the function
@param *rij_hist      Histogram of interatomic distances
@param *cfg           Parameters of simulation
@param *NatomEl       Array containing the total number of atoms of each chemical element
@param FF             X-ray atomic form-factor arrays for all chemical elements
@param SL             Array of neutron scattering lengths for all chemical elements
@param *q             Scattering vector magnitude array
@param Ntot           Total number of atoms in the nanoparticle
@param NumOMPthreads  Number of OpenMP threads
*/
void calcInt1DHist(double ** const I, unsigned long long int *rij_hist, const config * const cfg, const unsigned int * const NatomEl, const vector <double*> FF, const vector<double> SL, const double * const q, const unsigned int Ntot, const int NumOMPthreads, double frac);

/**
Depending on the computational scenario computes the scattering intensity (powder diffraction pattern) or PDF using the histogram of interatomic distances

@param **I            Scattering intensity array. The memory is allocated inside the function.
@param **PDF          PDF array. The memory is allocated inside the function.
@param *cfg           Parameters of simulation
@param *NatomEl       Array containing the total number of atoms of each chemical element
@param *ra            Atomic coordinate array
@param FF             X-ray atomic form-factor arrays for all chemical elements
@param SL             Array of neutron scattering lengths for all chemical elements
@param *q             Scattering vector magnitude array
@param Ntot           Total number of atoms in the nanoparticle
@param NumOMPthreads  Number of OpenMP threads
*/
void calcHistandPattern(double ** const I, const config * const cfg, const unsigned int * const NatomEl, const unsigned int * const NatomEl_outer, const vector < vect3d <float> > * const ra, const vector <double*> FF, const vector<double> SL, const double * const q, const unsigned int Ntot, const int NumOMPthreads, ostringstream *boinc_msg);

#endif

//Calculates rotational matrix using the Euler angles in the case of intristic rotation in the user - defined rotational convention.
//See http ://en.wikipedia.org/wiki/Euler_angles#Relationship_to_other_representations for details.
void calcRotMatrix(vect3d <double> * const RM0, vect3d <double> * const RM1, vect3d <double> * const RM2, const vect3d <double> euler, const unsigned int convention) {
	const vect3d <double> cosEul = cos(euler);
	const vect3d <double> sinEul = sin(euler);
	switch (convention){
		case EulerXZX: //XZX
			RM0->assign(cosEul.y,-cosEul.z*sinEul.y,sinEul.y*sinEul.z);
			RM1->assign(cosEul.x*sinEul.y,cosEul.x*cosEul.y*cosEul.z-sinEul.x*sinEul.z,-cosEul.z*sinEul.x-cosEul.x*cosEul.y*sinEul.z);
			RM2->assign(sinEul.x*sinEul.y,cosEul.x*sinEul.z+cosEul.y*cosEul.z*sinEul.x,cosEul.x*cosEul.z-cosEul.y*sinEul.x*sinEul.z);
			break;
		case EulerXYX: //XYX
			RM0->assign(cosEul.y, sinEul.y*sinEul.z, cosEul.z*sinEul.y);
			RM1->assign(sinEul.x*sinEul.y, cosEul.x*cosEul.z - cosEul.y*sinEul.x*sinEul.z, -cosEul.x*sinEul.z - cosEul.y*cosEul.z*sinEul.x);
			RM2->assign(-cosEul.x*sinEul.y, cosEul.z*sinEul.x + cosEul.x*cosEul.y*sinEul.z, cosEul.x*cosEul.y*cosEul.z - sinEul.x*sinEul.z);
			break;
		case EulerYXY: //YXY
			RM0->assign(cosEul.x*cosEul.z - cosEul.y*sinEul.x*sinEul.z, sinEul.x*sinEul.y, cosEul.x*sinEul.z + cosEul.y*cosEul.z*sinEul.x);
			RM1->assign(sinEul.y*sinEul.z, cosEul.y, -cosEul.z*sinEul.y);
			RM2->assign(-cosEul.z*sinEul.x - cosEul.x*cosEul.y*sinEul.z, cosEul.x*sinEul.y, cosEul.x*cosEul.y*cosEul.z - sinEul.x*sinEul.z);
			break;
		case EulerYZY: //YZY
			RM0->assign(cosEul.x*cosEul.y*cosEul.z - sinEul.x*sinEul.z, -cosEul.x*sinEul.y, cosEul.z*sinEul.x + cosEul.x*cosEul.y*sinEul.z);
			RM1->assign(cosEul.z*sinEul.y, cosEul.y, sinEul.y*sinEul.z);
			RM2->assign(-cosEul.x*sinEul.z - cosEul.y*cosEul.z*sinEul.x, sinEul.x*sinEul.y, cosEul.x*cosEul.z - cosEul.y*sinEul.x*sinEul.z);
			break;
		case EulerZYZ: //ZYZ
			RM0->assign(cosEul.x*cosEul.y*cosEul.z - sinEul.x*sinEul.z, -cosEul.z*sinEul.x - cosEul.x*cosEul.y*sinEul.z, cosEul.x*sinEul.y);
			RM1->assign(cosEul.x*sinEul.z + cosEul.y*cosEul.z*sinEul.x, cosEul.x*cosEul.z - cosEul.y*sinEul.x*sinEul.z, sinEul.x*sinEul.y);
			RM2->assign(-cosEul.z*sinEul.y, sinEul.y*sinEul.z, cosEul.y);
			break;
		case EulerZXZ: //ZXZ
			RM0->assign(cosEul.x*cosEul.z - cosEul.y*sinEul.x*sinEul.z, -cosEul.x*sinEul.z - cosEul.y*cosEul.z*sinEul.x, sinEul.x*sinEul.y);
			RM1->assign(cosEul.z*sinEul.x + cosEul.x*cosEul.y*sinEul.z, cosEul.x*cosEul.y*cosEul.z - sinEul.x*sinEul.z, -cosEul.x*sinEul.y);
			RM2->assign(sinEul.y*sinEul.z, cosEul.z*sinEul.y, cosEul.y);
			break;
		case EulerXZY: //XZY
			RM0->assign(cosEul.y*cosEul.z,-sinEul.y,cosEul.y*sinEul.z);
			RM1->assign(sinEul.x*sinEul.z+cosEul.x*cosEul.z*sinEul.y,cosEul.x*cosEul.y,cosEul.x*sinEul.y*sinEul.z-cosEul.z*sinEul.x);
			RM2->assign(cosEul.z*sinEul.x*sinEul.y-cosEul.x*sinEul.z,cosEul.y*sinEul.x,cosEul.x*cosEul.z+sinEul.x*sinEul.y*sinEul.z);
			break;
		case EulerXYZ: //XYZ
			RM0->assign(cosEul.y*cosEul.z, -cosEul.y*sinEul.z, sinEul.y);
			RM1->assign(cosEul.x*sinEul.z + cosEul.z*sinEul.x*sinEul.y, cosEul.x*cosEul.z - sinEul.x*sinEul.y*sinEul.z,-cosEul.y*sinEul.x);
			RM2->assign(sinEul.x*sinEul.z - cosEul.x*cosEul.z*sinEul.y,cosEul.z*sinEul.x + cosEul.x*sinEul.y*sinEul.z,cosEul.x*cosEul.y);
			break;
		case EulerYXZ: //YXZ
			RM0->assign(cosEul.x*cosEul.z + sinEul.x*sinEul.y*sinEul.z, cosEul.z*sinEul.x*sinEul.y - cosEul.x*sinEul.z,cosEul.y*sinEul.x);
			RM1->assign(cosEul.y*sinEul.z, cosEul.y*cosEul.z,-sinEul.y);
			RM2->assign(cosEul.x*sinEul.y*sinEul.z - cosEul.z*sinEul.x,cosEul.x*cosEul.z*sinEul.y+sinEul.x*sinEul.z,cosEul.x*cosEul.y);
			break;
		case EulerYZX: //YZX
			RM0->assign(cosEul.x*cosEul.y, sinEul.x*sinEul.z - cosEul.x*cosEul.z*sinEul.y, cosEul.z*sinEul.x+cosEul.x*sinEul.y*sinEul.z);
			RM1->assign(sinEul.y, cosEul.y*cosEul.z, -cosEul.y*sinEul.z);
			RM2->assign(-cosEul.y*sinEul.x, cosEul.x*sinEul.z + cosEul.z*sinEul.x*sinEul.y, cosEul.x*cosEul.z - sinEul.x*sinEul.y*sinEul.z);
			break;
		case EulerZYX: //ZYX
			RM0->assign(cosEul.x*cosEul.y, -cosEul.z*sinEul.x + cosEul.x*sinEul.z*sinEul.y, sinEul.x*sinEul.z + cosEul.x*cosEul.z*sinEul.y);
			RM1->assign(cosEul.y*sinEul.x, cosEul.x*cosEul.z + sinEul.x*sinEul.y*sinEul.z, -cosEul.x*sinEul.z + cosEul.z*sinEul.x*sinEul.y);
			RM2->assign(-sinEul.y, cosEul.y*sinEul.z, cosEul.y*cosEul.z);
			break;
		case EulerZXY: //ZXY
			RM0->assign(cosEul.x*cosEul.z - sinEul.x*sinEul.y*sinEul.z, -cosEul.y*sinEul.x, cosEul.x*sinEul.z+cosEul.z*sinEul.x*sinEul.y);
			RM1->assign(cosEul.z*sinEul.x + cosEul.x*sinEul.y*sinEul.z, cosEul.x*cosEul.y, sinEul.x*sinEul.z - cosEul.x*cosEul.z*sinEul.y);
			RM2->assign(-cosEul.y*sinEul.z, sinEul.y, cosEul.y*cosEul.z);
			break;
	}
}

//Returns the geometric center of the atomic ensemble (nanoparticle)
vect3d<float> GetCenter(const vector < vect3d<float> > * const r, const unsigned int Nel, const unsigned int Ntot){
	double rCx = 0, rCy = 0, rCz = 0;
	for (unsigned int iEl = 0; iEl < Nel; iEl++){
		for (vector<vect3d <float> >::const_iterator ri = r[iEl].begin(); ri != r[iEl].end(); ri++)	{
			rCx += (double)ri->x;
			rCy += (double)ri->y;
			rCz += (double)ri->z;
		}
	}
	return vect3d <float>((float)(rCx / Ntot), (float)(rCy / Ntot), (float)(rCz / Ntot));
}

//Returns the largest possible interatomic distance in the atomic ensemble (nanoperticle)
double GetEnsembleSize(const vector < vect3d<float> > * const r, const vect3d <float> rCenter, const unsigned int Nel) {
	double Rmax = 0;
	for (unsigned int iEl = 0; iEl < Nel; iEl++){
		for (vector<vect3d <float> >::const_iterator ri = r[iEl].begin(); ri != r[iEl].end(); ri++) Rmax = MAX(Rmax, (double)(rCenter - *ri).sqr());
	}
	return 2. * sqrt(Rmax);
}

//Returns the average atomic density of the atomic ensemble (nanoperticle)
double GetAtomicDensity(const vector < vect3d<float> > * const r, const vect3d <float> rCenter, const double Rcutoff, const unsigned int Nel){
	const double Rmax2 = SQR(Rcutoff);
	unsigned int count = 0;
	for (unsigned int iEl = 0; iEl < Nel; iEl++){
		for (vector<vect3d <float> >::const_iterator ri = r[iEl].begin(); ri != r[iEl].end(); ri++)	{
			if ((double)(rCenter - *ri).sqr() < Rmax2) count++;
		}
	}
	return count / (4. / 3. * PI * Rmax2 * sqrt(Rmax2));
}

//Returns the average atomic density of the atomic ensemble if cfg->BoxEdge parameters are defined
double GetAtomicDensityBox(const unsigned int N, const vect3d <double> *BoxEdge){
	const double dens = N / ABS(BoxEdge[0].dot(BoxEdge[1] * BoxEdge[2]));
	return dens;
}


//Moves the atoms located inside the inner cut-off box in the beginning of r[iEl] vector. Removes from r[iEl] the atoms locted outside the outer cut-off box
unsigned int cutoffBox(vector < vect3d <float> > * const r, const config * const cfg, unsigned int * const NatomEl, unsigned int * const NatomEl_outer){
	const double a = cfg->BoxEdge[0].mag(), b = cfg->BoxEdge[1].mag(), c = cfg->BoxEdge[2].mag();
	const vect3d <double> e0 = cfg->BoxEdge[0]/a, e1 = cfg->BoxEdge[1]/b, e2 = cfg->BoxEdge[2]/c;
	const double det = e0.x * (e1.y * e2.z - e1.z * e2.y) + e0.y * (e1.z * e2.x - e1.x * e2.z) + e0.z * (e1.x * e2.y - e1.y * e2.x);
	const vect3d <double> n2 = (e0 * e1).norm(), n0 = (e1 * e2).norm(), n1 = (e2 * e0).norm();
	const double aRcut = cfg->Rcutoff / e0.dot(n0), bRcut = cfg->Rcutoff / e1.dot(n1), cRcut = cfg->Rcutoff / e2.dot(n2);
	unsigned int Ntot = 0;
	for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++){
		vector < vect3d <float> > r_inner, r_outer;
		for (vector<vect3d <float> >::const_iterator ri = r[iEl].begin(); ri != r[iEl].end(); ri++) {
			const vect3d <double> rid((double)ri->x, (double)ri->y, (double)ri->z);
			const vect3d <double> rt = rid - cfg->BoxCorner;
			vect3d <double> rtb;			
			rtb.x = (rt.x * (e1.y * e2.z - e1.z * e2.y) + e0.y * (e1.z * rt.z - rt.y * e2.z) + e0.z * (rt.y * e2.y - e1.y * rt.z)) / det;
			rtb.y = (e0.x * (rt.y * e2.z - e1.z * rt.z) + rt.x * (e1.z * e2.x - e1.x * e2.z) + e0.z * (e1.x * rt.z - rt.y * e2.x)) / det;
			rtb.z = (e0.x * (e1.y * rt.z - rt.y * e2.y) + e0.y * (rt.y * e2.x - e1.x * rt.z) + rt.x * (e1.x * e2.y - e1.y * e2.x)) / det;
			if ((rtb.x >= 0) && (rtb.x < a) && (rtb.y >= 0) && (rtb.y < b) && (rtb.z >= 0) && (rtb.z < c)) r_inner.push_back(*ri);
			else if ((rtb.x >= -aRcut) && (rtb.x < a + aRcut) && (rtb.y >= -bRcut) && (rtb.y < b + bRcut) && (rtb.z >= -cRcut) && (rtb.z < c + cRcut)) r_outer.push_back(*ri);
		}
		NatomEl[iEl] = (unsigned int)r_inner.size();
		Ntot += NatomEl[iEl];
		NatomEl_outer[iEl] = NatomEl[iEl] + (unsigned int)r_outer.size();
		r[iEl].clear();
		r[iEl].insert(r[iEl].end(), r_inner.begin(), r_inner.end());
		r[iEl].insert(r[iEl].end(), r_outer.begin(), r_outer.end());
	}
	return Ntot;
}

//Moves the atoms located inside the inner cut-off sphere in the beginning of r[iEl] vector. Removes from r[iEl] the atoms locted outside the outer cut-off sphere
unsigned int cutoffSphere(vector < vect3d <float> > * const r, const config * const cfg, unsigned int * const NatomEl, unsigned int * const NatomEl_outer, const vect3d <float> rCenter){
	const double Ri2 = SQR(cfg->Rsphere);
	const double Ro2 = SQR(cfg->Rsphere + cfg->Rcutoff);
	unsigned int Ntot = 0;
	for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++){
		vector < vect3d <float> > r_inner, r_outer;
		for (vector<vect3d <float> >::const_iterator ri = r[iEl].begin(); ri != r[iEl].end(); ri++) {
			const double dist2 = (double)(rCenter - *ri).sqr();
			if (dist2 < Ri2) r_inner.push_back(*ri);
			else if (dist2 < Ro2) r_outer.push_back(*ri);
		}
		NatomEl[iEl] = (unsigned int) r_inner.size();
		Ntot += NatomEl[iEl];
		NatomEl_outer[iEl] = NatomEl[iEl] + (unsigned int) r_outer.size();
		r[iEl].clear();
		r[iEl].insert(r[iEl].end(), r_inner.begin(), r_inner.end());
		r[iEl].insert(r[iEl].end(), r_outer.begin(), r_outer.end());
	}
	return Ntot;
}

//Calculates the total atomic ensemble and prints it in the .xyz file if cfg.PrintAtoms is true. 
//Returns the total number of atoms in the atomic ensemble(nanoparticle).
unsigned int CalcAndPrintAtoms(config * const cfg, block * const Block, vector < vect3d <float> > ** const ra, unsigned int ** const NatomEl, unsigned int ** const NatomEl_outer, const map <string, unsigned int> ID, const map <unsigned int, string> EN, ostringstream *boinc_msg){
	unsigned int Ntot = 0;
	*ra = new vector < vect3d <float> >[cfg->Nel];
	*NatomEl = new unsigned int[cfg->Nel];
	if (cfg->cutoff) *NatomEl_outer = new unsigned int[cfg->Nel];
	chrono::steady_clock::time_point t1 = chrono::steady_clock::now();
	for (unsigned int iB = 0; iB<cfg->Nblocks; iB++) Block[iB].calcAtoms(*ra, ID, cfg->Nel);
	for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++) {
		(*NatomEl)[iEl] = (unsigned int)(*ra)[iEl].size();
		Ntot += (*NatomEl)[iEl];
	}
	const vect3d <float> rCenter = GetCenter(*ra, cfg->Nel, Ntot);
	double D = GetEnsembleSize(*ra, rCenter, cfg->Nel);
	if (cfg->cutoff) {
		if (cfg->sphere) {
			if (D < 2. * (cfg->Rsphere + cfg->Rcutoff)) cout << "/nWarning: the sum of sphere radius and cutoff radius " << (cfg->Rsphere + cfg->Rcutoff) << " A is to big for the atomic ensemble with the linear size " << D << " A.\n" << endl;
			Ntot = cutoffSphere(*ra, cfg, *NatomEl, *NatomEl_outer, rCenter);
		}
		else Ntot = cutoffBox(*ra, cfg, *NatomEl, *NatomEl_outer);
		D = cfg->Rcutoff;
	}
	if (cfg->PrintAtoms)	printAtoms(*ra, *NatomEl, cfg->name, EN, Ntot);
	cfg->Nhist = (unsigned int)(D / cfg->hist_bin) + 1;
	if ((cfg->cutoff) && (!cfg->p0)) {
		if (cfg->cutoff) {
			if (cfg->sphere) cfg->p0 = GetAtomicDensity(*ra, rCenter, cfg->Rcutoff, cfg->Nel);
			else cfg->p0 = GetAtomicDensityBox(Ntot, cfg->BoxEdge);
		}
		else cfg->p0 = GetAtomicDensity(*ra, rCenter, 0.25 * D, cfg->Nel);
		cout << "Approximate atomic density of the sample is " << cfg->p0 << " A^(-3).\n" << endl;
	}
	const chrono::steady_clock::time_point t2 = chrono::steady_clock::now();
	cout << "Atomic ensemble calculation time: " << chrono::duration_cast< chrono::duration < float > >(t2 - t1).count() << " s\n" << endl;
	*boinc_msg << "Atomic ensemble calculation time: " << chrono::duration_cast<chrono::duration < float >>(t2 - t1).count() << " s\n\n";
	return Ntot;
}

#if !defined(UseOCL) && !defined(UseCUDA)

//Computes the polarization factor and multiplies scattering intensity by this factor
void PolarFactor1D(double * const I, const double * const q, const config * const cfg){
	for (unsigned int iq = 0; iq<cfg->q.N; iq++){
		const double sintheta = q[iq] * (cfg->lambda * 0.25 / PI);
		const double cos2theta = 1. - 2. * SQR(sintheta);
		I[iq] *= 0.5 * (1. + SQR(cos2theta));
	}
}


//Adds the average density correction to the scattering intensity when the cut-off is enabled (cfg.cutoff == true)
void AddCutoff(double * const I, const config * const cfg, const unsigned int * const NatomEl, const double * const q, const vector <double*> FF, const vector<double> SL, const unsigned int Ntot){
	double FFaver = 0;
	if (cfg->source == neutron) {
		for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++)	FFaver += SL[iEl] * NatomEl[iEl];
		FFaver /= Ntot;
	}	
	for (unsigned int iq = 0; iq < cfg->q.N; iq++) {
		if (q[iq] < 1.e-7) continue;
		if (cfg->source == xray) {
			FFaver = 0;
			for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++)	FFaver += FF[iEl][iq] * NatomEl[iEl];
			FFaver /= Ntot;
		}
		const double qrcut = q[iq] * cfg->Rcutoff;
		if (cfg->damping) I[iq] += 4. * PI * cfg->p0 * SQR(FFaver) * SQR(cfg->Rcutoff) * sin(qrcut) / (q[iq] * (SQR(qrcut) - SQR(PI)));
		else I[iq] += 4. * PI * cfg->p0 * SQR(FFaver) * (cfg->Rcutoff * cos(qrcut) - sin(qrcut) / q[iq]) / SQR(q[iq]);
	}
}

//Computes the contribution to the histogram of interatomic distances from the atoms of the chemical elements with indeces iEl and jEl
void HistCore(unsigned long long int * const __restrict rij_hist, const config * const __restrict cfg, const vector < vect3d <float> > * const __restrict ra, const unsigned int NatomEli, const unsigned int NatomElj, const unsigned int iEl, const unsigned int jEl, const unsigned int id, const int NumOMPthreads, double *Nop_done, double Nop_total, const double frac_hist) {
	unsigned int step = 2 * id + 1;
	unsigned int count = 0;
	unsigned int jAtomST = 0;
	float hist_bin = (float)cfg->hist_bin;
	for (unsigned int iAtom = id; iAtom < NatomEli; iAtom += step, count++) {
		(count % 2) ? step = 2 * id + 1 : step = 2 * (NumOMPthreads - id) - 1;
		if (jEl == iEl) jAtomST = iAtom + 1;
		for (unsigned int jAtom = jAtomST; jAtom < NatomElj; jAtom++) {
			const float rij = (ra[iEl][iAtom] - ra[jEl][jAtom]).mag();
			rij_hist[(unsigned int)(rij / hist_bin)] += 2;
		}
		if (!id) {
			*Nop_done += (double)(NumOMPthreads)* (double)(NatomElj - jAtomST);
			if (!(count % 50)) boinc_fraction_done(0.01 + frac_hist * MIN(1.,*Nop_done / Nop_total));
			//if (!(count % 50)) cout << 0.01 + frac_hist * MIN(1.,*Nop_done / Nop_total) << endl;
		}
	}
}

//Computes the contribution to the histogram of interatomic distances from the atoms of the chemical elements with indeces iEl and jEl (if cfg->cutoff == true)
void HistCoreCutOff(unsigned long long int * const __restrict rij_hist, const config * const __restrict  cfg, const vector < vect3d <float> > * const __restrict ra, const unsigned int NatomEli, const unsigned int NatomElj, const unsigned int NatomElj_out, const unsigned int iEl, const unsigned int jEl, const unsigned int id, const int NumOMPthreads, double *Nop_done, double Nop_total, const double frac_hist) {
	unsigned int step = 2 * id + 1;
	unsigned int count = 0;
	unsigned int jAtomST = 0;
	if (jEl < iEl) jAtomST = NatomElj;
	float hist_bin = (float)cfg->hist_bin;
	const float Rcutoff2 = (float) SQR(cfg->Rcutoff);
	for (unsigned int iAtom = id; iAtom < NatomEli; iAtom += step, count++) {
		(count % 2) ? step = 2 * id + 1 : step = 2 * (NumOMPthreads - id) - 1;
		if (jEl == iEl) jAtomST = iAtom + 1;
		for (unsigned int jAtom = jAtomST; jAtom < NatomElj; jAtom++) {
			const float rij2 = (ra[iEl][iAtom] - ra[jEl][jAtom]).sqr();
			if (rij2 > Rcutoff2) continue;
			rij_hist[(unsigned int)(sqrt(rij2) / hist_bin)] += 2;
		}
		for (unsigned int jAtom = NatomElj; jAtom < NatomElj_out; jAtom++) {
			const float rij2 = (ra[iEl][iAtom] - ra[jEl][jAtom]).sqr();
			if (rij2 > Rcutoff2) continue;
			rij_hist[(unsigned int)(sqrt(rij2) / hist_bin)] += 1;
		}
		if (!id) {
			*Nop_done += (double)(NumOMPthreads)* (double)(NatomElj_out - jAtomST);
			if (!(count % 50)) boinc_fraction_done(0.01 + frac_hist * MIN(1.,*Nop_done / Nop_total));
			//if (!(count % 50)) cout << 0.01 + frac_hist * MIN(1.,*Nop_done / Nop_total) << endl;
		}
	}
}

//Computes the histogram of interatomic distances
double calcHist(unsigned long long int ** const rij_hist, const config *const cfg, const vector < vect3d <float> > * const ra, const unsigned int * const NatomEl, const unsigned int * const NatomEl_outer, const int NumOMPthreads) {
	const unsigned int NhistEl = (cfg->Nel * (cfg->Nel + 1)) / 2 * cfg->Nhist;
	*rij_hist = new unsigned long long int[NhistEl * NumOMPthreads];
	for (unsigned int i = 0; i < NhistEl * NumOMPthreads; i++) (*rij_hist)[i] = 0;
	unsigned int Ntot_i = 0;
	unsigned int Ntot_j = 0;	
	for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++) {
		Ntot_i += NatomEl[iEl];
		(cfg->cutoff) ? Ntot_j += NatomEl_outer[iEl] : Ntot_j += NatomEl[iEl];
	}
	const double Nop_total = double(Ntot_i) * (double(Ntot_j) - 0.5 * double(Ntot_i));
	double frac_hist = 0.98;	
	if (cfg->uncert) {
		if (cfg->cutoff) {
			const double time_hist = 1.25e-11 * Nop_total;
			const double time_pattern = 4.29e-11 * NhistEl * cfg->q.N;
			frac_hist = 0.98 * time_hist / (time_pattern + time_hist);
		}
		else {
			const double time_hist = 2.12e-11 * Nop_total;
			const double time_pattern = 1.35e-11 * NhistEl * cfg->q.N;
			frac_hist = 0.98 * time_hist / (time_pattern + time_hist);
		}
	}
	double Nop_done = 0;
	//cout << "Number of operations: " << Nop_total << ". Histogram size: " << NhistEl << endl;
#ifdef UseOMP
#pragma omp parallel num_threads(NumOMPthreads) 
#endif
{
	int tid = 0;
#ifdef UseOMP
	tid = omp_get_thread_num();
#endif	
	const unsigned int NstartID = NhistEl * tid;
	for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++) {
		unsigned int jElSt = iEl;
		if (cfg->cutoff) jElSt = 0;
		for (unsigned int jEl = jElSt; jEl < cfg->Nel; jEl++) {
			unsigned int Nstart = NstartID;
			(jEl > iEl) ? Nstart += cfg->Nhist * (cfg->Nel * iEl - (iEl * (iEl + 1)) / 2 + jEl) : Nstart += cfg->Nhist * (cfg->Nel * jEl - (jEl * (jEl + 1)) / 2 + iEl);
			if (cfg->cutoff) HistCoreCutOff(*rij_hist + Nstart, cfg, ra, NatomEl[iEl], NatomEl[jEl], NatomEl_outer[jEl], iEl, jEl, tid, NumOMPthreads, &Nop_done, Nop_total, frac_hist);
			else HistCore(*rij_hist + Nstart, cfg, ra, NatomEl[iEl], NatomEl[jEl], iEl, jEl, tid, NumOMPthreads, &Nop_done, Nop_total, frac_hist);
		}
	}
}
#ifdef UseOMP
#pragma omp parallel for num_threads(NumOMPthreads) 
for (int i = 0; i < (int) NhistEl; i++) {
	for (int tid = 1; tid < NumOMPthreads; tid++) (*rij_hist)[i] += (*rij_hist)[tid * NhistEl + i];
}
#endif
	return frac_hist;
}

void Int1DHistCore(double * const __restrict  I, const unsigned long long int * const __restrict rij_hist, const config * const __restrict cfg, const double * const __restrict q, const unsigned int Nhist, const unsigned int iStart, const int NumOMPthreads, const unsigned int Nstart, const double frac){
	const unsigned int NhistEl = (cfg->Nel * (cfg->Nel + 1)) / 2 * cfg->Nhist;
#ifdef UseOMP
#pragma omp parallel num_threads(NumOMPthreads) 
#endif
	{
		int tid = 0;
#ifdef UseOMP
		tid = omp_get_thread_num();
#endif
		for (unsigned int iq = 0; iq < cfg->q.N; iq++) I[tid * cfg->q.N + iq] = 0;
		double damp = 1.;
		for (int i = tid; i < (int)Nhist; i += NumOMPthreads) {
			if ((!tid) && (!(i % 5000)) && (cfg->uncert)) {
				const double frac_done = 0.01 + frac + (0.98 - frac) * (double)(Nstart + i) / NhistEl;
				boinc_fraction_done(frac_done);
				//cout << frac_done << endl;
			}
			if (!rij_hist[i]) continue;			
			const double rij = ((double)(iStart + i) + 0.5) * cfg->hist_bin;
			if (cfg->damping) {
				const double x = PI * rij / cfg->Rcutoff;
				damp = sin(x) / x;
			}
			for (unsigned int iq = 0; iq < cfg->q.N; iq++) {
				const double qrij = rij * q[iq] + 0.00000001;
				I[tid * cfg->q.N + iq] += damp * rij_hist[i] * sin(qrij) / qrij;
			}
		}
	}
}

//Computes the scattering intensity (powder diffraction pattern) using the histogram of interatomic distances
void calcInt1DHist(double ** const I, unsigned long long int *rij_hist, const config * const cfg, const unsigned int * const NatomEl, const vector <double*> FF, const vector<double> SL, const double * const q, const unsigned int Ntot, const int NumOMPthreads, double frac) {
	unsigned int Nhist0 = cfg->Nhist;
	unsigned int Nstart = 0;
	*I = new double[cfg->q.N];
	for (unsigned int iq = 0; iq < cfg->q.N; iq++) (*I)[iq] = 0;
	double * const Itemp = new double[cfg->q.N * NumOMPthreads];
	for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++) {
		for (unsigned int jEl = iEl; jEl < cfg->Nel; jEl++, Nstart += Nhist0) {
			Int1DHistCore(Itemp, rij_hist + Nstart, cfg, q, cfg->Nhist, 0, NumOMPthreads, Nstart, frac);
#ifdef UseOMP
#pragma omp parallel for num_threads(NumOMPthreads) 
			for (int iq = 0; iq < (int) cfg->q.N; iq++) {
				for (int tid = 1; tid < NumOMPthreads; tid++) Itemp[iq] += Itemp[tid * cfg->q.N + iq];
			}
#endif
			if (cfg->source == xray) {
				for (unsigned int iq = 0; iq < cfg->q.N; iq++) {
					(*I)[iq] += Itemp[iq] * FF[iEl][iq] * FF[jEl][iq];
				}
			}
			else {
				for (unsigned int iq = 0; iq < cfg->q.N; iq++) {
					(*I)[iq] += Itemp[iq] * SL[iEl] * SL[jEl];
				}
			}
		}
	}
	delete[] Itemp;
	for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++) {
		if (cfg->source == xray) {
			for (unsigned int iq = 0; iq < cfg->q.N; iq++) (*I)[iq] += NatomEl[iEl] * SQR(FF[iEl][iq]);
		}
		else {
			for (unsigned int iq = 0; iq < cfg->q.N; iq++) (*I)[iq] += NatomEl[iEl] * SQR(SL[iEl]);
		}
	}
	for (unsigned int iq = 0; iq < cfg->q.N; iq++) (*I)[iq] /= Ntot;
	if (cfg->cutoff) AddCutoff(*I, cfg, NatomEl, q, FF, SL, Ntot);
	if (cfg->PolarFactor) PolarFactor1D(*I, q, cfg);
}


//Depending on the computational scenario computes the scattering intensity (powder diffraction pattern) or PDF using the histogram of interatomic distances
void calcHistandPattern(double ** const I, const config * const cfg, const unsigned int * const NatomEl, const unsigned int * const NatomEl_outer, const vector < vect3d <float> > * const ra, const vector <double*> FF, const vector<double> SL, const double * const q, const unsigned int Ntot, const int NumOMPthreads, ostringstream *boinc_msg) {
	chrono::steady_clock::time_point t1 = chrono::steady_clock::now();
	unsigned long long int *rij_hist = NULL;
	const double frac_done = calcHist(&rij_hist, cfg, ra, NatomEl, NatomEl_outer, NumOMPthreads);
	chrono::steady_clock::time_point t2 = chrono::steady_clock::now();
	cout << "Histogram calculation time: " << chrono::duration_cast< chrono::duration < float > >(t2 - t1).count() << " s" << endl;
	*boinc_msg << "Histogram calculation time: " << chrono::duration_cast<chrono::duration < float >>(t2 - t1).count() << " s\n";	
	t1 = chrono::steady_clock::now();
	calcInt1DHist(I, rij_hist, cfg, NatomEl, FF, SL, q, Ntot, NumOMPthreads, frac_done);
	t2 = chrono::steady_clock::now();
	cout << "1D pattern calculation time: " << chrono::duration_cast< chrono::duration < float > >(t2 - t1).count() << " s" << endl;
	*boinc_msg << "1D pattern calculation time: " << chrono::duration_cast< chrono::duration < float > >(t2 - t1).count() << " s\n";
	delete [] rij_hist;
}
#endif
