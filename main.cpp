//Copyright (C) 2017, NRC "Kurchatov institute", http://www.nrcki.ru/e/engl.html, Moscow, Russia
//Author: Vladislav Neverov, vs-never@hotmail.com, neverov_vs@nrcki.ru
//
//This file is part of XaNSoNS BOINC.
//
//XaNSoNS BOINC is free software: you can redistribute it and / or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//XaNSoNS BOINC is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program. If not, see <http://www.gnu.org/licenses/>.

//main file
#include "config.h"
#include "block.h"
#include <chrono>
#ifdef UseOMP
#include <omp.h>
#endif
#ifdef UseOCL
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#include "boinc_opencl.h"
#else
#include <CL/cl.h>
#include "boinc_opencl.h"
#endif
#endif

struct float4;

int ReadConfig(config * const cfg, double ** const q, block ** const Block, const char * const FileName, map<string, unsigned int> * const ID, map<unsigned int, string> * const EN, vector<double> * const SL, vector<double *> * const FF, ostringstream *boinc_msg);//ReadXML.cpp
unsigned int CalcAndPrintAtoms(config * const cfg, block * const Block, vector < vect3d <float> > ** const ra, unsigned int ** const NatomEl, unsigned int ** const NatomEl_outer, const map <string, unsigned int> ID, const map <unsigned int, string> EN, ostringstream *boinc_msg);//CalcFunctions.cpp
int printI(const double * const I, const double * const q, const config * const cfg);//IO.cpp

#ifdef UseCUDA
void calcHistandPatternCuda(const int DeviceNUM, double ** const I, const config * const cfg, const unsigned int * const NatomEl, const unsigned int * const NatomEl_outer, const float4 * const ra, const float * const dFF, const vector<double> SL, const float * const dq, ostringstream *boinc_msg);//CalcFunctionsCUDA.cu
void delDataFromDevice(float4 * const ra, float * const dFF, float * const dq, const unsigned int Nel);//CalcFunctionsCUDA.cu
void dataCopyCUDA(const double *const q, const config * const cfg, const vector < vect3d <float> > * const ra, float4 ** const dra, float ** const dFF, float ** const dq, const vector <double*> FF);//CalcFunctionsCUDA.cu
int SetDeviceCuda(const int DeviceNUM, ostringstream *boinc_msg);//CalcFunctionsCUDA.cu

#elif UseOCL

void ShowDeviceOCL(const cl_device_id OCLdevice, ostringstream *boinc_msg);
int createContextOCL(cl_context * const OCLcontext, cl_program * const OCLprogram, const cl_device_id OCLdevice, const char * const argv0, ostringstream *boinc_msg); //CalcFunctionsOCL.cpp
void dataCopyOCL(const cl_context OCLcontext, const cl_device_id OCLdevice, const double * const q, const config *const cfg, const vector < vect3d <float> > *const ra, cl_mem * const dra, cl_mem * const dFF, cl_mem * const dq, const vector <double*> FF); //CalcFunctionsOCL.cpp
void delDataFromDeviceOCL(const cl_context OCLcontext, const cl_program OCLprogram, const cl_mem ra, const cl_mem dFF, const cl_mem dq, const unsigned int Nel); //CalcFunctionsOCL.cpp
void calcHistandPatternOCL(const cl_context OCLcontext, const cl_device_id OCLdevice, const cl_program OCLprogram, double ** const I, const config * const cfg, const unsigned int * const NatomEl, const unsigned int * const NatomEl_outer, const cl_mem ra, const cl_mem dFF, const vector<double> SL, const cl_mem dq, ostringstream *boinc_msg); //CalcFunctionsOCL.cpp
#else

void calcHistandPattern(double ** const I, const config * const cfg, const unsigned int * const NatomEl, const unsigned int * const NatomEl_outer, const vector < vect3d <float> > * const ra, const vector <double*> FF, const vector<double> SL, const double * const q, const unsigned int Ntot, const int NumOMPthreads, ostringstream *boinc_msg);//CalcFunctions.cpp

#endif

int FinalizeIfError(const unsigned int source, double * const q, block * const Block, const vector<double *> * const FF, ostringstream *boinc_msg) {
	delete[] q;
	delete[] Block;
	if (source == xray)	for (unsigned int i = 0; i<FF->size(); i++)	delete[] (*FF)[i];
	cout << "\nTerminating with error. See stdout for the details" << endl;
	ofstream boinc_err;
	boinc_err.open("stderr.txt", ofstream::app);
	if (boinc_err.is_open()){
		boinc_err << (*boinc_msg).str();
		boinc_err.close();
	}
	return boinc_finish(-1);
}

#if defined(_MSC_VER) && (_MSC_VER >= 1400)
void AppInvalidParameterHandler(const wchar_t* expression, const wchar_t* function, const wchar_t* file, unsigned int line, uintptr_t pReserved ) {
	DebugBreak();
}
#endif

int main(int argc, char *argv[]){
#if defined(_MSC_VER) && (_MSC_VER >= 1400)
	_set_invalid_parameter_handler(AppInvalidParameterHandler);
#endif
#ifdef UseOMP
	int NumOMPthreads = 1;
	BOINC_OPTIONS options;
	boinc_options_defaults(options);
	options.multi_thread = true;
	boinc_init_options(&options);
    for (int i = 1; i < argc; i++) {
        if (!strcmp(argv[i], "--nthreads")) {
            NumOMPthreads = atoi(argv[++i]);
			break;
        }
    }
#elif UseCUDA
	int DeviceNUM = 0;
	BOINC_OPTIONS options;
	boinc_options_defaults(options);
	options.normal_thread_priority = true; 
	boinc_init_options(&options);
	APP_INIT_DATA aid;
	boinc_get_init_data(aid);

#elif UseOCL
	cl_device_id OCLdevice;
	cl_platform_id OCLplatform;
	BOINC_OPTIONS options;
	boinc_options_defaults(options);
	options.normal_thread_priority = true; 
	boinc_init_options(&options);
	boinc_get_opencl_ids(argc, argv, 0, &OCLdevice, &OCLplatform);
#else
	int NumOMPthreads = 1;
	boinc_init();
#endif
	string resolved_stdout;
	boinc_resolve_filename_s("stdout.txt", resolved_stdout);
	ofstream out(resolved_stdout);
	cout.rdbuf(out.rdbuf());
	ostringstream boinc_msg;
	boinc_msg << "Command line arguments recieved: ";
	for (int i = 1; i < argc; i++) boinc_msg << argv[i] << " ";
	boinc_msg << "\n\n";
#ifdef UseCUDA
	if (aid.gpu_device_num >= 0) {
		DeviceNUM = aid.gpu_device_num;
		boinc_msg << "CUDA device number #"<< DeviceNUM << " is provided by APP_INIT_DATA structure.\n";
		boinc_msg << "Ignoring --device arguments.\n\n";
	}
	else {
		for (int i = 1; i < argc; i++) {
			if (!strcmp(argv[i], "--device")) {
				DeviceNUM = atoi(argv[++i]);
				break;
			}
		}
	}
#endif
	
#ifdef UseOMP	
	cout << "Number of OpenMP threads is set to " << NumOMPthreads << endl;
	boinc_msg << "Number of OpenMP threads is set to " << NumOMPthreads << "\n\n";
#endif
	config cfg;
	block *Block = NULL;
	double *q = NULL;
	map<string, unsigned int> ID;
	map<unsigned int, string> EN;
	vector<double> SL;
	vector<double *> FF;
	for (int i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "--nowait")) {
			cfg.wait4kernel = false;
			break;
		}
	}
	int error = ReadConfig(&cfg, &q, &Block, "start.xml", &ID, &EN, &SL, &FF, &boinc_msg);// reading the calculation parameters, from - factors, etc., creating structural blocks
	if (error) return FinalizeIfError(cfg.source, q, Block, &FF, &boinc_msg);
	chrono::steady_clock::time_point t1, t2;
	t1 = chrono::steady_clock::now();
	unsigned int *NatomEl = NULL, *NatomEl_outer = NULL;
	vector < vect3d <float> > *ra = NULL;
	const unsigned int Ntot = CalcAndPrintAtoms(&cfg, Block, &ra, &NatomEl, &NatomEl_outer, ID, EN, &boinc_msg);//creating the atomic ensemble
#ifdef UseCUDA
	float *dq = NULL, *dFF = NULL;
	float4 *dra = NULL;//using float4 structure for three coordinates to assure for global memory access coalescing
	error = SetDeviceCuda(DeviceNUM, &boinc_msg);//queries CUDA devices, changes DeviceNUM to proper device number if required
	if (error) return FinalizeIfError(cfg.source, q, Block, &FF, &boinc_msg);
	dataCopyCUDA(q, &cfg, ra, &dra, &dFF, &dq, FF);//copying all the necessary data to the device memory
	delete[] ra;
#elif UseOCL
	cl_mem dq = NULL, dFF = NULL, dra = NULL;
	cl_context OCLcontext;
	cl_program OCLprogram = NULL;
	ShowDeviceOCL(OCLdevice, &boinc_msg);
	error = createContextOCL(&OCLcontext, &OCLprogram, OCLdevice, argv[0], &boinc_msg);//copying all the necessary data to the device memory 
	if (error) return FinalizeIfError(cfg.source, q, Block, &FF, &boinc_msg);
	dataCopyOCL(OCLcontext, OCLdevice, q, &cfg, ra, &dra, &dFF, &dq, FF);
	delete[] ra;
#endif
	delete[] Block;
	double *I = NULL;
	boinc_fraction_done(0.01);
#ifdef UseCUDA
	calcHistandPatternCuda(DeviceNUM, &I, &cfg, NatomEl, NatomEl_outer, dra, dFF, SL, dq, &boinc_msg);
#elif UseOCL
	calcHistandPatternOCL(OCLcontext, OCLdevice, OCLprogram, &I, &cfg, NatomEl, NatomEl_outer, dra, dFF, SL, dq, &boinc_msg);
#else
	calcHistandPattern(&I, &cfg, NatomEl, NatomEl_outer, ra, FF, SL, q, Ntot, NumOMPthreads, &boinc_msg);
	delete[] ra;
#endif
	t2 = chrono::steady_clock::now();
	cout << "Total calculation time: " << chrono::duration_cast< chrono::duration < float > >(t2-t1).count() << " s" << endl;
	boinc_msg << "Total calculation time: " << chrono::duration_cast< chrono::duration < float > >(t2 - t1).count() << " s" << endl;
	ofstream boinc_err;
	boinc_err.open("stderr.txt", ofstream::app);
	if (boinc_err.is_open()){
		boinc_err << boinc_msg.str();
		boinc_err.close();
	}
	if (cfg.source == xray)	for (unsigned int i = 0; i<FF.size(); i++)	delete[] FF[i];
	delete[] NatomEl;
	delete[] NatomEl_outer;
#ifdef UseCUDA
	delDataFromDevice(dra, dFF, dq, cfg.Nel);//deleting the data from the device memory
#elif UseOCL
	delDataFromDeviceOCL(OCLcontext, OCLprogram, dra, dFF, dq, cfg.Nel);
#endif
	printI(I, q, &cfg);
	delete[] I;
	delete[] q;	
	boinc_fraction_done(1.);
	return boinc_finish(0);
}
