//Copyright (C) 2017, NRC "Kurchatov institute", http://www.nrcki.ru/e/engl.html, Moscow, Russia
//Author: Vladislav Neverov, vs-never@hotmail.com, neverov_vs@nrcki.ru
//
//This file is part of XaNSoNS BOINC.
//
//XaNSoNS BOINC is free software: you can redistribute it and / or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//XaNSoNS BOINC is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program. If not, see <http://www.gnu.org/licenses/>.

//Defines function of config class

#include "config.h"
#include "ReadXML_utils.h"

//Set the parameters to their defaults
void config::SetDefault(){
	source = xray;
	Nblocks = 0;
	q.N = 1024;
	PolarFactor = false;
	PrintAtoms = false;
	cutoff = false;
	sphere = false;
	damping = false;
	uncert = false;
	wait4kernel = true;
	EulerConvention = EulerZXZ;
	lambda = -1.;
	Rcutoff = 0;
	Rsphere = 0;
	hist_bin = 0.001;
	p0 = 0.;
	BoxCorner.assign(0, 0, 0);
	BoxEdge[0].assign(0, 0, 0);
	BoxEdge[1].assign(0, 0, 0);
	BoxEdge[2].assign(0, 0, 0);
}

//Reads properties of the parallelepiped box if cutoff is true.
int config::ReadBoxProps(tinyxml2::XMLElement *boxNode){
	if (!boxNode) {
		sphere = true;
		return 0;
	}
	sphere = false;
	int error = 0;
	error += GetAttribute(boxNode, "Box", "a", BoxEdge[0], false);
	error += GetAttribute(boxNode, "Box", "b", BoxEdge[1], false);
	error += GetAttribute(boxNode, "Box", "c", BoxEdge[2], false);
	error += GetAttribute(boxNode, "Box", "corner", BoxCorner, false);
	const double Vol = ABS(BoxEdge[0].dot(BoxEdge[1] * BoxEdge[2]));
	if (Vol < 1.e-8) {
		cout << "Error. Box has zero volume. Check Calculation-->Sample-->Box-->a(b,c) values." << endl;
		error -= 1;
	}
	return error;
}


//Reads the parameters required for "Debye_hist" scenario
int config::ReadParametersDebye_hist(tinyxml2::XMLElement *calcNode){
	if (!calcNode) return -1;
	map<string, bool> flag;
	flag["yes"] = true; flag["no"] = false; flag["true"] = true; flag["false"] = false; flag["1"] = true; flag["0"] = false;
	int error = 0;
	if (source == xray) error += GetWord(calcNode, "Calculation", "PolarFactor", flag, PolarFactor, true, "No");
	if (PolarFactor) error += GetAttribute(calcNode, "Calculation", "wavelength", lambda, false);
	else error += GetAttribute(calcNode, "Calculation", "wavelength", lambda, true, "default");
	if (lambda < 0) lambda = 4.*PI / q.max;
	error += GetAttribute(calcNode, "Calculation", "hist_bin", hist_bin, true, "0.001");
	tinyxml2::XMLElement *tempNode = calcNode->FirstChildElement("q");
	error += GetAttribute(tempNode, "q", "min", q.min, false);
	error += GetAttribute(tempNode, "q", "max", q.max, false);
	error += GetAttribute(tempNode, "q", "N", q.N, true, "1024");
	tempNode = calcNode->FirstChildElement("Sample");
	error += GetAttribute(tempNode, "Sample", "Rcutoff", Rcutoff, true, "0");
	if (Rcutoff > 0.5) cutoff = true;
	if (cutoff) {
		Rsphere = Rcutoff;
		error += GetAttribute(tempNode, "Sample", "density", p0, true, "approximately calculated value");
		error += GetWord(tempNode, "Sample", "damping", flag, damping, true, "No");
		error += ReadBoxProps(tempNode->FirstChildElement("Box"));
		if (sphere) error += GetAttribute(tempNode, "Sample", "Rsphere", Rsphere, true, "cutoff radius");
	}
	return error;
}

//Reads the parameters of calculation
int config::ReadParameters(tinyxml2::XMLElement *calcNode){
	if (!calcNode) return -1;
	map<string, unsigned int> word;
	word["xray"] = xray; word["neutron"] = neutron;
	map<string, bool> flag;
	flag["yes"] = true; flag["no"] = false; flag["true"] = true; flag["false"] = false; flag["1"] = true; flag["0"] = false;
	int error = 0;
	error += GetAttribute(calcNode, "Calculation", "name", name, false);
	error += GetWord(calcNode, "Calculation", "source", word, source, true, "Xray");
	error += GetWord(calcNode, "Calculation", "PrintAtoms", flag, PrintAtoms, true, "No");
	error += GetAttribute(calcNode, "Calculation", "FFfilename", FFfilename, false);
	error += ReadParametersDebye_hist(calcNode);
	return error;
}