//Copyright (C) 2017, NRC "Kurchatov institute", http://www.nrcki.ru/e/engl.html, Moscow, Russia
//Author: Vladislav Neverov, vs-never@hotmail.com, neverov_vs@nrcki.ru
//
//This file is part of XaNSoNS BOINC.
//
//XaNSoNS BOINC is free software: you can redistribute it and / or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//XaNSoNS BOINC is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program. If not, see <http://www.gnu.org/licenses/>.

//Contains host and device code for the CUDA version of XaNSoNS

#ifndef __CUDACC__
#define __CUDACC__
#endif
#include "typedefs.h"
#ifdef UseCUDA

#include "config.h"
#include "block.h"
#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

//Calculates rotational matrix (from CalcFunctions.cpp)
void calcRotMatrix(vect3d <double> * const RM0, vect3d <double> * const RM1, vect3d <double> * const RM2, const vect3d <double> euler, const unsigned int convention);

//some float4 and float 3 functions (float4 used as float3)
inline __device__ __host__ float dot(float3 a, float3 b) { return a.x * b.x + a.y * b.y + a.z * b.z; }
inline __device__ __host__ float dot(float3 a, float4 b) { return a.x * b.x + a.y * b.y + a.z * b.z; }
inline __device__ __host__ float dot(float4 a, float3 b) { return a.x * b.x + a.y * b.y + a.z * b.z; }
inline __host__ __device__ float3 operator+(float3 a, float3 b){ return make_float3(a.x + b.x, a.y + b.y, a.z + b.z); }
inline __host__ __device__ float3 operator-(float3 a, float3 b){ return make_float3(a.x - b.x, a.y - b.y, a.z - b.z); }
inline __host__ __device__ float3 operator*(float3 a, float b){ return make_float3(a.x * b, a.y * b, a.z * b); }
inline __device__ float length(float3 v){ return sqrtf(dot(v, v)); }

//the following functions are used to calculate the histogram of interatomic distances

/**
	Resets the histogram array (unsigned long long int)

	@param *rij_hist  Histogram of interatomic distances
	@param N          Size of the array
*/
__global__ void zeroHistKernel(unsigned long long int * const rij_hist, const unsigned int N);

/**
	Computes the total histogram (first Nhist elements) using the partial histograms (for the devices with the CUDA compute capability < 2.0)

	@param *rij_hist   Partial histograms of interatomic distances
	@param Nhistcopies Number of the partial histograms to sum
	@param Nfin        Number of bins to compute for one kernel call
*/
__global__ void sumHistKernel(unsigned long long int * const rij_hist, const unsigned int Nhistcopies, const unsigned int Nfin, const unsigned int Nhist);

/**
	Computes the histogram of interatomic distances

	@param *ri         Pointer to the coordinate of the 1st i-th atom in ra array
	@param *rj         Pointer to the coordinate of the 1st j-th atom in ra array
	@param iMax        Total number of i-th atoms for this kernel call
	@param jMax        Total number of j-th atoms for this kernel call
	@param *rij_hist   Histogram of interatomic distances
	@param bin         Width of the histogram bin
	@param Nhistcopies Number of partial histograms to compute (!=1 for the devices with the CUDA compute capability < 2.0 to reduce the number of atomicAdd() calls)
	@param Nhist       Size of the partial histogram of interatomic distances
	@param Rcut2       Square of the cut-off radius (if cfg->cutoff is true)
	@param add         Addendum to the histogram bin. Equals to 2 if cutoff is false, otherwise equals to 2 if j-th atoms belong to the inner sphere or to 1 if to the outer
	@param diag        True if the j-th atoms and the i-th atoms are the same (diagonal) for this kernel call
*/
template <unsigned int BlockSize2D, bool cutoff> __global__ void calcHistKernel(const float4 * const __restrict__ ri, const float4 *  const __restrict__ rj, const unsigned int iMax, const unsigned int jMax, unsigned long long int *const rij_hist, const float bin, const unsigned int Nhistcopies, const unsigned int Nhist, const float Rcut2, const unsigned long long int add, const bool diag);

/**
	Organazies the computations of the histogram of interatomic distances with CUDA 

	@param DeviceNUM   CUDA device number
	@param **rij_hist  Histogram of interatomic distances (device). The memory is allocated inside the function.
	@param *ra         Atomic coordinate array (device)
	@param *NatomEl    Array containing the total number of atoms of each chemical element (host)
	@param *NatomEl_outer  Array containing the number of atoms of each chemical element including the atoms in the outer sphere (only if cfg.cutoff is True) (host)
	@param *cfg      Parameters of simulation	
*/
void calcHistCuda(const int DeviceNUM, unsigned long long int ** const rij_hist, const float4 * const ra, const unsigned int * const NatomEl, const unsigned int * const NatomEl_outer, const config * const cfg, ostringstream *boinc_msg);

//the following functions are used to calculate the powder diffraction pattern using the histogram of interatomic distances

/**
	Resets 1D float array of size N

	@param *A  Array
	@param N   Size of the array	
*/
__global__ void zero1DFloatArrayKernel(float * const A, const unsigned int N);

/**
	Computes the total scattering intensity (first Nq elements) from the partials sums computed by different thread blocks

	@param *I    Scattering intensity array
	@param Nq    Resolution of the total scattering intensity (powder diffraction pattern) 
	@param Nsum  Number of parts to sum (equalt to the total number of thread blocks in the grid)
*/
__global__ void sumIKernel(float * const I, const unsigned int Nq, const unsigned int Nsum);

/**
	Adds the diagonal elements (j==i) of the Debye double sum to the x-ray scattering intensity 

	@param *I    Scattering intensity array
	@param *FF   X-ray atomic form-factor array (for one kernel call the computations are done only for the atoms of the same chemical element)
	@param Nq    Resolution of the total scattering intensity (powder diffraction pattern)
	@param N     Total number of atoms of the chemical element for whcich the computations are done 
*/
__global__ void addIKernelXray(float * const I, const float * const FF, const unsigned int Nq, const unsigned int N);

/**
	Adds the diagonal elements (j==i) of the Debye double sum to the neutron scattering intensity 

	@param *I    Scattering intensity array
	@param Nq    Resolution of the total scattering intensity (powder diffraction pattern)
	@param Add   The value to add to the intensity (the result of multiplying the square of the scattering length 
                 to the total number of atoms of the chemical element for whcich the computations are done) 
*/
__global__ void addIKernelNeutron(float * const I, const unsigned int Nq, const float Add);

/**
	Computes the polarization factor and multiplies scattering intensity by this factor

	@param *I     Scattering intensity array
	@param Nq     Size of the scattering intensity array
	@param *q     Scattering vector magnitude array
	@param lambda Wavelength of the source
*/
__global__ void PolarFactor1DKernel(float * const I, const unsigned int Nq, const float * const q, const float lambda);

/**
	Computes the x-ray (source == xray) or neutron (source == neutron) scattering intensity (powder diffraction pattern) using the histogram of interatomic distances

	@param source          xray or neutron
	@param *I              Scattering intensity array
	@param *FFi            X-ray atomic form factor for the i-th atoms (all the i-th atoms are of the same chemical element for one kernel call) (NULL if source is neutron)
	@param *FFj            X-ray atomic form factor for the j-th atoms (all the j-th atoms are of the same chemical element for one kernel call)(NULL if source is neutron)
	@param SLij            Product of the scattering lenghts of i-th j-th atoms (0 if source is xray)
	@param *q              Scattering vector magnitude array
	@param Nq              Size of the scattering intensity array
	@param **rij_hist      Histogram of interatomic distances (device). The memory is allocated inside the function
	@param iBinSt          Starting index of the histogram bin for this kernel call (the kernel is called iteratively in a loop)
	@param Nhist           Size of the partial histogram of interatomic distances
	@param MaxBinsPerBlock Maximum number of histogram bins used by a single thread block
	@param bin             Width of the histogram bin
	@param Rcut            Cutoff radius in A (if cutoff is true)
	@param damping         cfg->damping
*/
template <unsigned int Size> __global__ void calcIntHistKernel(const unsigned int source, float * const I, const float * const FFi, const float * const FFj, const float SLij, const float *const q, const unsigned int Nq, const unsigned long long int *const rij_hist, const unsigned int iBinSt, const unsigned int Nhist, const unsigned int MaxBinsPerBlock, const float bin, const float Rcut, const bool damping);

/**
    Adds the average density correction to the xray scattering intensity when the cut-off is enabled (cfg.cutoff == true)

	@param *I        Scattering intensity array
	@param *q        Scattering vector magnitude array
	@param Nq        Size of the scattering intensity array
	@param *dFF      X-ray atomic form-factor array for all chemical elements
	@param *NatomEl  Array containing the total number of atoms of each chemical element
	@param Nel       Total number of different chemical elements in the nanoparticle
	@param Ntot      Total number of atoms in the nanoparticle
	@param Rcut      Cutoff radius in A (if cutoff is true)
	@param dens      Average atomic density of the nanoparticle
	@param damping   cfg->damping
*/
__global__ void AddCutoffKernelXray(float * const I, const float * const q, const float * const FF, const unsigned int * const NatomEl, const unsigned int Nel, const unsigned int Ntot, const unsigned int Nq, const float Rcut, const float dens, const bool damping);

/**
    Adds the average density correction to the neutron scattering intensity when the cut-off is enabled (cfg.cutoff == true)

	@param *I        Scattering intensity array
	@param *q        Scattering vector magnitude array
	@param Nq        Size of the scattering intensity array
	@param SLaver    Average neutron scattering length of the nanopaticle
	@param Rcut      Cutoff radius in A (if cutoff is true)
	@param dens      Average atomic density of the nanoparticle
	@param damping   cfg->damping
*/
__global__ void AddCutoffKernelNeutron(float * const I, const float * const q, const float SLaver, const unsigned int Nq, const float Rcut, const float dens, const bool damping);

/**
    Adds the average density correction to the scattering intensity when the cut-off is enabled (cfg.cutoff == true)

	@param GSadd     Grid size for AddCutoffKernel... kernels
	@param *dI       Scattering intensity array (device)
	@param *NatomEl  Array containing the total number of atoms of each chemical element (host)
	@param *cfg      Parameters of simulation
	@param *dFF      X-ray atomic form-factor array for all chemical elements (device)
	@param SL        Array of neutron scattering lengths for all chemical elements
	@param *dq       Scattering vector magnitude array (device)
	@param Ntot      Total number of atoms in the nanoparticle
*/
void AddCutoffCUDA(const unsigned int GSadd, float * const dI, const unsigned int *const NatomEl, const config * const cfg, const float * const dFF, const vector<double> SL, const float * const dq, const unsigned int Ntot);

/**
	Organazies the computations of the scattering intensity (powder diffraction pattern) using the histogram of interatomic distances with CUDA

	@param DeviceNUM CUDA device number
	@param **I       Scattering intensity array (host). The memory is allocated inside the function
	@param *rij_hist Histogram of interatomic distances (device).
	@param *NatomEl  Array containing the total number of atoms of each chemical element (host)
	@param *cfg      Parameters of simulation
	@param *dFF      X-ray atomic form-factor array for all chemical elements (device)
	@param SL        Array of neutron scattering lengths for all chemical elements
	@param *dq       Scattering vector magnitude array (device)
	@param Ntot      Total number of atoms in the nanoparticle
*/
void calcInt1DHistCuda(const int DeviceNUM, double ** const I, const unsigned long long int * const rij_hist, const unsigned int *const NatomEl, const config * const cfg, const float * const dFF, const vector<double> SL, const float * const dq, const unsigned int Ntot);

/**
	Depending on the computational scenario organazies the computations of the scattering intensity (powder diffraction pattern) or PDF using the histogram of interatomic distances with CUDA

	@param DeviceNUM       CUDA device number
	@param **I             Scattering intensity array (host). The memory is allocated inside the function.
	@param *cfg            Parameters of simulation
	@param *NatomEl        Array containing the total number of atoms of each chemical element (host)
	@param *NatomEl_outer  Array containing the number of atoms of each chemical element including the atoms in the outer sphere (only if cfg.cutoff is True) (host)
	@param *ra             Atomic coordinate array (device)
	@param *dFF            X-ray atomic form-factor array for all chemical elements (device)
	@param SL              Array of neutron scattering lengths for all chemical elements
	@param *dq             Scattering vector magnitude array (device)
*/
void calcHistandPatternCuda(const int DeviceNUM, double ** const I, const config * const cfg, const unsigned int * const NatomEl, const unsigned int * const NatomEl_outer, const float4 * const ra, const float * const dFF, const vector<double> SL, const float * const dq, ostringstream *boinc_msg);

//the following functions are used to set the CUDA device, copy/delete the data to/from the device memory

/**
	Queries all CUDA devices. Checks and sets the CUDA device number
	Returns 0 if OK and -1 if no CUDA devices found

	@param DeviceNUM CUDA device number
*/
int SetDeviceCuda(const int DeviceNUM, ostringstream *boinc_msg);

/**
	Copies the atomic coordinates (ra), scattering vector magnitude (q) and the x-ray atomic form-factors (FF) to the device memory	

	@param *q      Scattering vector magnitude (host)
	@param *cfg    Parameters of simulation
	@param *ra     Atomic coordinates (host)
	@param **dra   Atomic coordinates (device). The memory is allocated inside the function
	@param **dFF   X-ray atomic form-factors (device). The memory is allocated inside the function
	@param **dq    Scattering vector magnitude (device). The memory is allocated inside the function
	@param FF      X-ray atomic form-factors (host)
*/
void dataCopyCUDA(const double *const q, const config * const cfg, const vector < vect3d <double> > * const ra, float4 ** const dra, float ** const dFF, float ** const dq, const vector <double*> FF);

/**
	Deletes the atomic coordinates (ra), scattering vector magnitude (dq) and the x-ray atomic form-factors (dFF) from the device memory

	@param *ra    Atomic coordinates (device)
	@param *dFF   X-ray atomic form-factors (device)
	@param *dq    Scattering vector magnitude (device)
	@param Nel    Total number of different chemical elements in the nanoparticle
*/
void delDataFromDevice(float4 * const ra, float * const dFF, float * const dq, const unsigned int Nel);

/**
	Returns the theoretical peak performance of the CUDA device

	@param deviceProp  Device properties object
	@param show        If True, show the device information on screen
*/
unsigned int GetGFLOPS(const cudaDeviceProp deviceProp, const bool show, ostringstream *boinc_msg);

//Returns the theoretical peak performance of the CUDA device
unsigned int GetGFLOPS(const cudaDeviceProp deviceProp, const bool show = false, ostringstream *boinc_msg = NULL){
	const unsigned int cc = deviceProp.major * 10 + deviceProp.minor; //compute capability
	const unsigned int MP = deviceProp.multiProcessorCount; //number of multiprocessors
	const unsigned int clockRate = deviceProp.clockRate / 1000; //GPU clockrate
	unsigned int ALUlanes = 64;
	switch (cc){
	case 10:
	case 11:
	case 12:
	case 13:
		ALUlanes = 8;
		break;
	case 20:
		ALUlanes = 32;
		break;
	case 21:
		ALUlanes = 48;
		break;
	case 30:
	case 35:
	case 37:
		ALUlanes = 192;
		break;
	case 50:
	case 52:
		ALUlanes = 128;
		break;
	case 60:
		ALUlanes = 64;
		break;
	case 61:
	case 62:
		ALUlanes = 128;
		break;
	case 70:
	case 72:
	case 75:
		ALUlanes = 64;
		break;
	}
	unsigned int GFLOPS = MP * ALUlanes * 2 * clockRate / 1000;
	if (show) {
		cout << "GPU name: " << deviceProp.name << "\n";
		cout << "CUDA compute capability: " << deviceProp.major << "." << deviceProp.minor << "\n";
		cout << "Number of multiprocessors: " << MP << "\n";
		cout << "GPU clock rate: " << clockRate << " MHz" << "\n";
		cout << "Theoretical peak performance: " << GFLOPS << " GFLOPs\n" << endl;
		if (boinc_msg) {
			*boinc_msg << "GPU name: " << deviceProp.name << "\n";
			*boinc_msg << "CUDA compute capability: " << deviceProp.major << "." << deviceProp.minor << "\n";
			*boinc_msg << "Number of multiprocessors: " << MP << "\n";
			*boinc_msg << "GPU clock rate: " << clockRate << " MHz" << "\n";
			*boinc_msg << "Theoretical peak performance: " << GFLOPS << " GFLOPs\n\n";
		}
	}
	return GFLOPS;
}

//Computes polarization factor and multiplies scattering intensity by this factor
__global__ void PolarFactor1DKernel(float * const I, const unsigned int Nq, const float * const q, const float lambda){
	const unsigned int iq = blockIdx.x * blockDim.x + threadIdx.x;
	if (iq < Nq)	{
		const float sintheta = q[iq] * (lambda * 0.25f / PIf);
		const float cos2theta = 1.f - 2.f * SQR(sintheta);
		const float factor = 0.5f * (1.f + SQR(cos2theta));
		I[blockIdx.y * Nq + iq] *= factor;
	}
}

//Resets 1D float array of size N
__global__ void zero1DFloatArrayKernel(float * const A, const unsigned int N){
	const unsigned int i = blockDim.x * blockIdx.x + threadIdx.x;
	if (i<N) A[i]=0;
}

//Adds the diagonal elements(j == i) of the Debye double sum to the x - ray scattering intensity
__global__ void addIKernelXray(float * const I, const float * const FF, const unsigned int Nq, const unsigned int N) {
	const unsigned int iq = blockIdx.x * blockDim.x + threadIdx.x;
	if (iq < Nq)	{
		const float lFF = FF[iq];
		I[iq] += SQR(lFF) * N;
	}
}

//Adds the diagonal elements(j == i) of the Debye double sum to the neutron scattering intensity
__global__ void addIKernelNeutron(float * const I, const unsigned int Nq, const float Add) {
	const unsigned int iq = blockIdx.x * blockDim.x + threadIdx.x;
	if (iq < Nq)	I[iq] += Add;
}

//Computes the total scattering intensity (first Nq elements) from the partials sums computed by different thread blocks
__global__ void sumIKernel(float * const I, const unsigned int Nq, const unsigned int Nsum){
	const unsigned int iq = blockDim.x * blockIdx.x + threadIdx.x;
	if (iq<Nq) {
		for (unsigned int j = 1; j < Nsum; j++)	I[iq] += I[j * Nq + iq];
	}
}

//Resets the histogram array (unsigned long long int)
__global__ void zeroHistKernel(unsigned long long int * const rij_hist, const unsigned int N){
	const unsigned int i=blockDim.x * blockIdx.x + threadIdx.x;
	if (i<N) rij_hist[i]=0;
}	

//Computes the histogram of interatomic distances
template <unsigned int BlockSize2D, bool cutoff> __global__ void calcHistKernel(const float4 *  const __restrict__ ri, const float4 *  const __restrict__ rj, const unsigned int iMax, const unsigned int jMax, unsigned long long int *const rij_hist, const float bin, const unsigned int Nhistcopies, const unsigned int Nhist, const float Rcut2, const unsigned long long int add, const bool diag){
	if ((diag) && (blockIdx.x < blockIdx.y)) return; //we need to calculate inter-atomic distances only for j > i, so if we are in the diagonal grid, all the subdiagonal blocks (for which j < i for all threads) do nothing and return
	const unsigned int jt = threadIdx.x, it = threadIdx.y;
	const unsigned int j = blockIdx.x * BlockSize2D + jt;
	const unsigned int iCopy = blockIdx.y * BlockSize2D + jt; //jt!!! memory transaction are performed by the threads of the same warp to coalesce them
	const unsigned int i = blockIdx.y * BlockSize2D + it;
	unsigned int copyind = 0;
	if (Nhistcopies > 1) copyind = ((it * BlockSize2D + jt) % Nhistcopies) * Nhist; //some optimization for CC < 2.0. Making multiple copies of the histogram array reduces the number of atomicAdd() operations on the same elements.
	__shared__ float4 ris[BlockSize2D], rjs[BlockSize2D]; //cache arrays for atomic coordinates 
	if ((it == 0) && (j < jMax)) rjs[jt] = rj[j]; //copying atomic coordinates for j-th (column) atoms (only the threads of the first half-warp are used)
	if ((it == 2) && (iCopy < iMax)) ris[jt] = ri[iCopy]; //the same for i-th (row) atoms (only the threads of the first half-warp of the second warp for CC < 2.0 are used)
	__syncthreads(); //sync to ensure that copying is complete
	if ((j < jMax) && (i < iMax) && ((j > i) || (!diag))) {
		const float rij2 = SQR(ris[it].x - rjs[jt].x) + SQR(ris[it].y - rjs[jt].y) + SQR(ris[it].z - rjs[jt].z);//calculate square of distance	
		if (cutoff){	
			if (rij2 < Rcut2) {
				const unsigned int index = (unsigned int)(sqrtf(rij2) / bin); //get the index of histogram bin
				atomicAdd(&rij_hist[copyind + index], add); //add +2 or +1 to histogram bin
			}
		}
		else {
			const unsigned int index = (unsigned int)(sqrtf(rij2) / bin); //get the index of histogram bin
			atomicAdd(&rij_hist[copyind + index], add); //add +1 to histogram bin
		}
	}
}

//Computes the total histogram (first Nhist elements) using the partial histograms (for the devices with the CUDA compute capability < 2.0)
__global__ void sumHistKernel(unsigned long long int * const rij_hist, const unsigned int Nhistcopies, const unsigned int Nfin, const unsigned int Nhist){
	const unsigned int i = blockDim.x * blockIdx.x + threadIdx.x;
	if (i < Nfin){
		for (unsigned int iCopy = 1; iCopy < Nhistcopies; iCopy++)	rij_hist[i] += rij_hist[Nhist * iCopy + i];
	}
}

//Organazies the computations of the histogram of interatomic distances with CUDA 
void calcHistCuda(const int DeviceNUM, unsigned long long int ** const rij_hist, const float4 * const ra, const unsigned int * const NatomEl, const unsigned int * const NatomEl_outer, const config * const cfg, ostringstream *boinc_msg){
	const unsigned int BlockSize = BlockSize1Dsmall, BlockSize2D = BlockSize2Dsmall; //size of the thread blocks (256, 16x16)
	const unsigned int NhistEl = (cfg->Nel * (cfg->Nel + 1)) / 2 * cfg->Nhist;//Number of partial (Element1<-->Element2) histograms
	cudaDeviceProp deviceProp;
	cudaGetDeviceProperties(&deviceProp, DeviceNUM); //getting the device properties
	const int cc = deviceProp.major * 10 + deviceProp.minor; //device compute capability
	unsigned int Nhistcopies = 1;
	if (cc<20){//optimization for the devices with CC < 2.0
		//atomic operations work very slow for the devices with Tesla architecture as compared with the modern devices
		//we minimize the number of atomic operations on the same elements by making multiple copies of pair-distribution histograms
		size_t free, total;
		cuMemGetInfo(&free, &total); //checking the amount of the free GPU memory	
		Nhistcopies = MIN(BlockSize,(unsigned int)(0.25 * float(free) / (NhistEl * sizeof(unsigned long long int)))); //set optimal number for histogram copies 
		if (!Nhistcopies) Nhistcopies = 1;
	}
	unsigned int GridSizeExecMax = 2048;
	const unsigned int GFLOPS = GetGFLOPS(deviceProp); //theoretical peak GPU performance
	if (deviceProp.kernelExecTimeoutEnabled)	{//killswitch is enabled, so the time limit should not be exceeded
		const double tmax = 0.02; //maximum kernel time execution in seconds
		const double k = 1.e-6; // t = k * GridSizeExecMax^2 * BlockSize2D^2 / GFLOPS
		GridSizeExecMax = MIN((unsigned int)(sqrt(tmax * GFLOPS / k) / BlockSize2D), GridSizeExecMax);
	}
	else {
		*boinc_msg << "\nKernel execution timeout is disabled for this GPU\n";
	}
	//total histogram size is equal to the product of: partial histogram size for one pair of elements (Nhist), number of partial histograms ((Nel*(Nel + 1)) / 2), number of histogram copies (Nhistcopies)
	const unsigned int NhistTotal = NhistEl * Nhistcopies;
	cudaError err = cudaMalloc(rij_hist, NhistTotal * sizeof(unsigned long long int));//trying to allocate large amount of memory, check for errors
	if (err != cudaSuccess) cout << "Error in calcHistCuda(), cudaMalloc(): " << cudaGetErrorString(err) << endl;
	const unsigned int GSzero = MIN(65535, NhistTotal / BlockSize + BOOL(NhistTotal % BlockSize));//Size of the grid for zeroHistKernel (it could not be large than 65535)
	//reseting pair-distribution histogram array
	boinc_begin_critical_section();
	for (unsigned int iter = 0; iter < NhistTotal / BlockSize + BOOL(NhistTotal % BlockSize); iter += GSzero)	zeroHistKernel << < GSzero, BlockSize >> >(*rij_hist + iter*BlockSize, NhistTotal - iter*BlockSize);
	//cudaThreadSynchronize();//synchronizing before the calculation starts
	boinc_end_critical_section();
	dim3 blockgrid(BlockSize2D, BlockSize2D);//2D thread block size
	const float4 * * const raEl = new const float4*[cfg->Nel];
	raEl[0] = ra;
	unsigned int Ntot_i = NatomEl[0];
	unsigned int Ntot_j = NatomEl[0];
	if (cfg->cutoff) Ntot_j = NatomEl_outer[0];
	for (unsigned int iEl = 1; iEl < cfg->Nel; iEl++) {
		Ntot_i += NatomEl[iEl];
		(cfg->cutoff) ? Ntot_j += NatomEl_outer[iEl] : Ntot_j += NatomEl[iEl];
		(cfg->cutoff) ? raEl[iEl] = raEl[iEl - 1] + NatomEl_outer[iEl - 1] : raEl[iEl] = raEl[iEl - 1] + NatomEl[iEl - 1];
	}
	double Nop_total = double(Ntot_i) * (double(Ntot_j) - 0.5 * double(Ntot_i));
	double Nop_done = 0;
	unsigned int count = 0;
	const float bin = float(cfg->hist_bin);
	const float Rcut2 = float(SQR(cfg->Rcutoff));
	for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++) {
		unsigned int jElSt = iEl;
		if (cfg->cutoff) jElSt = 0;
		for (unsigned int jEl = jElSt; jEl < cfg->Nel; jEl++) {//each time we move to the next pair of elements (iEl,jEl) we also move to the respective part of histogram (Nstart += Nhist)
			unsigned int jAtomST = 0;
			if ((cfg->cutoff) && (jEl < iEl)) jAtomST = NatomEl[jEl];
			unsigned int Nstart = 0;
			(jEl > iEl) ? Nstart = cfg->Nhist * (cfg->Nel * iEl - (iEl * (iEl + 1)) / 2 + jEl) : Nstart = cfg->Nhist * (cfg->Nel * jEl - (jEl * (jEl + 1)) / 2 + iEl);
			for (unsigned int iAtom = 0; iAtom < NatomEl[iEl]; iAtom += BlockSize2D * GridSizeExecMax){
				const unsigned int GridSizeExecY = MIN((NatomEl[iEl] - iAtom) / BlockSize2D + BOOL((NatomEl[iEl] - iAtom) % BlockSize2D), GridSizeExecMax);//Y-size of the grid on the current step
				const unsigned int iMax = MIN(BlockSize2D * GridSizeExecY, NatomEl[iEl] - iAtom);//index of the last i-th (row) atom
				if (iEl == jEl) jAtomST = iAtom;//loop should exclude subdiagonal grids
				for (unsigned int jAtom = jAtomST; jAtom < NatomEl[jEl]; jAtom += BlockSize2D * GridSizeExecMax){
					const unsigned int GridSizeExecX = MIN((NatomEl[jEl] - jAtom) / BlockSize2D + BOOL((NatomEl[jEl] - jAtom) % BlockSize2D), GridSizeExecMax);//X-size of the grid on the current step
					const unsigned int jMax = MIN(BlockSize2D * GridSizeExecX, NatomEl[jEl] - jAtom);//index of the last j-th (column) atom
					dim3 grid(GridSizeExecX, GridSizeExecY);
					bool diag = false;
					if ((iEl == jEl) && (iAtom == jAtom)) diag = true;//checking if we are on the diagonal grid or not
					/*float time;
					cudaEvent_t start, stop;
					cudaEventCreate(&start);
					cudaEventCreate(&stop);
					cudaEventRecord(start, 0);*/
					boinc_begin_critical_section();
					if (cfg->cutoff) calcHistKernel <BlockSize2Dsmall, true> << <grid, blockgrid >> >(raEl[iEl] + iAtom, raEl[jEl] + jAtom, iMax, jMax, *rij_hist + Nstart, bin, Nhistcopies, NhistEl, Rcut2, 2, diag);
					else calcHistKernel <BlockSize2Dsmall, false> << <grid, blockgrid >> >(raEl[iEl] + iAtom, raEl[jEl] + jAtom, iMax, jMax, *rij_hist + Nstart, bin, Nhistcopies, NhistEl, 0, 2, diag);
					if (cfg->wait4kernel) cudaThreadSynchronize();//the kernel above uses atomic operation, it's hard to predict the execution time of a single kernel, so sync to avoid the killswitch triggering 
					boinc_end_critical_section();
					Nop_done += (1. - diag * 0.5) * (double)(iMax * jMax);
					if (!(count % 10)) boinc_fraction_done(0.01 + 0.98 * Nop_done / Nop_total);
					//if (!(count % 10)) cout << 0.01 + 0.98 * Nop_done / Nop_total << endl;
					count++;
					/*cudaEventRecord(stop, 0);
					cudaEventSynchronize(stop);
					cudaEventElapsedTime(&time, start, stop);
					cout << "calcHistKernel execution time is: " << time << " ms\n" << endl;*/
				}
				if (cfg->cutoff) {
					for (unsigned int jAtom = NatomEl[jEl]; jAtom < NatomEl_outer[jEl]; jAtom += BlockSize2D * GridSizeExecMax){
						unsigned int GridSizeExecX = MIN((NatomEl_outer[jEl] - jAtom) / BlockSize2D + BOOL((NatomEl_outer[jEl] - jAtom) % BlockSize2D), GridSizeExecMax);//X-size of the grid on the current step
						unsigned int jMax = MIN(BlockSize2D * GridSizeExecX, NatomEl_outer[jEl] - jAtom);//index of the last j-th (column) atom
						dim3 grid(GridSizeExecX, GridSizeExecY);
						boinc_begin_critical_section();
						calcHistKernel <BlockSize2Dsmall, true> << <grid, blockgrid >> >(raEl[iEl] + iAtom, raEl[jEl] + jAtom, iMax, jMax, *rij_hist + Nstart, bin, Nhistcopies, NhistEl, Rcut2, 1, false);
						if (cfg->wait4kernel) cudaThreadSynchronize();//the kernel above uses atomic operation, it's hard to predict the execution time of a single kernel, so sync to avoid the killswitch triggering 
						boinc_end_critical_section();
						Nop_done += (double)(iMax * jMax);
						if (!(count % 10)) boinc_fraction_done(0.01 + 0.98 * Nop_done / Nop_total);
						//if (!(count % 10)) cout << 0.01 + 0.98 * Nop_done / Nop_total << endl;
						count++;
					}
				}
			}
		}
	}
	cudaThreadSynchronize();//synchronizing to ensure that all calculations ended before histogram copies summation starts
	delete[] raEl;
	boinc_begin_critical_section();
	if (Nhistcopies>1) {//summing the histogram copies
		const unsigned int GSsum = MIN(65535, NhistEl / BlockSize + BOOL(NhistEl % BlockSize));
		for (unsigned int iter = 0; iter < NhistEl / BlockSize + BOOL(NhistEl % BlockSize); iter += GSsum)	sumHistKernel << <GSsum, BlockSize >> >(*rij_hist + iter * BlockSize, Nhistcopies, NhistEl - iter * BlockSize, NhistEl);
	}
	cudaThreadSynchronize();//synchronizing before the further usage of histogram in other functions
	boinc_end_critical_section();
}

//Computes the x-ray (source == xray) or neutron (source == neutron) scattering intensity (powder diffraction pattern) using the histogram of interatomic distances
template <unsigned int Size> __global__ void calcIntHistKernel(const unsigned int source, float * const I, const float * const FFi, const float * const FFj, const float SLij, const float *const q, const unsigned int Nq, const unsigned long long int *const rij_hist, const unsigned int iBinSt, const unsigned int Nhist, const unsigned int MaxBinsPerBlock, const float bin, const float Rcut, const bool damping){
	__shared__ long long int Nrij[Size];//cache array for the histogram
	__shared__ float damp[Size];
	Nrij[threadIdx.x] = 0;
	damp[threadIdx.x] = 1.;
	__syncthreads();
	const unsigned int iBegin = iBinSt + blockIdx.x * MaxBinsPerBlock;//first index for histogram bin to process
	const unsigned int iEnd = MIN(Nhist, iBegin + MaxBinsPerBlock);//last index for histogram bin to process
	if (iEnd < iBegin) return;
	const unsigned int Niter = (iEnd - iBegin) / blockDim.x + BOOL((iEnd - iBegin) % blockDim.x);//number of iterations
	for (unsigned int iter = 0; iter < Niter; iter++){//we don't have enough shared memory to load the histogram array as a whole, so we do it with iterations
		const unsigned int NiterFin = MIN(iEnd - iBegin - iter * blockDim.x, blockDim.x);//maximum number of histogram bins on current iteration step
		if (threadIdx.x < NiterFin) {
			const unsigned int index = iBegin + iter * blockDim.x + threadIdx.x;
			Nrij[threadIdx.x] = rij_hist[index]; //loading the histogram array to shared memory
			if (damping) {
				const float rij = ((float)(index) + 0.5f) * bin;//distance that corresponds to the current histogram bin
				const float x = PIf * rij / Rcut;
				damp[threadIdx.x] = __sinf(x) / x;
			}
		}
		__syncthreads();//synchronizing after loading
		for (unsigned int iterq = 0; iterq < (Nq / blockDim.x) + BOOL(Nq % blockDim.x); iterq++) {//if Nq > blockDim.x there will be threads that compute more than one element of the intensity array
			const unsigned int iq = iterq * blockDim.x + threadIdx.x;//index of the intensity array element
			if (iq < Nq) {//checking for the array margin				
				const float lq = q[iq];//copying the scattering vector magnitude to the local memory
				float lI = 0;
				for (unsigned int i = 0; i < NiterFin; i++) {//looping over the histogram bins
					if (Nrij[i]){
						const float qrij = lq * ((float)(iBegin + iter * blockDim.x + i) + 0.5f) * bin + 0.000001f;//distance that corresponds to the current histogram bin
						lI += Nrij[i] * damp[i] * __sinf(qrij) / qrij;//scattering intensity without form factors
					}
				}
				if (source == xray) I[blockIdx.x * Nq + iq] += lI * FFi[iq] * FFj[iq];//multiplying intensity by form-factors and storing the results in global memory
				else I[blockIdx.x * Nq + iq] += lI * SLij;
			}
		}
		__syncthreads();//synchronizing threads before the next iteration step
	}
}

//Adds the average density correction to the xray scattering intensity when the cut-off is enabled (cfg.cutoff == true)
__global__ void AddCutoffKernelXray(float * const I, const float * const q, const float * const FF, const unsigned int * const NatomEl, const unsigned int Nel, const unsigned int Ntot, const unsigned int Nq, const float Rcut, const float dens, const bool damping){
	const unsigned int iq = blockIdx.x * blockDim.x + threadIdx.x;
	if (iq < Nq) {
		float FFaver = 0;
		for (unsigned int iEl = 0; iEl < Nel; iEl++) FFaver += FF[iEl * Nq + iq] * NatomEl[iEl];
		FFaver /= Ntot;
		const float lq = q[iq];
		if (lq > 0.000001f) {
			const float qrcut = lq * Rcut;
			if (damping) I[iq] += 4.f * PIf * Ntot * dens * SQR(FFaver) * SQR(Rcut) * __sinf(qrcut) / (lq * (SQR(qrcut) - SQR(PIf)));
			else I[iq] += 4.f * PIf * Ntot * dens * SQR(FFaver) * (Rcut * __cosf(qrcut) - __sinf(qrcut) / lq) / SQR(lq);
		}
	}
}

//Adds the average density correction to the neutron scattering intensity when the cut-off is enabled (cfg.cutoff == true)
__global__ void AddCutoffKernelNeutron(float * const I, const float * const q, const float SLaver, const unsigned int Ntot, const unsigned int Nq, const float Rcut, const float dens, const bool damping){
	const unsigned int iq = blockIdx.x * blockDim.x + threadIdx.x;
	if (iq < Nq) {
		const float lq = q[iq];
		if (lq > 0.000001f) {
			const float qrcut = lq * Rcut;
			if (damping) I[iq] += 4.f * PIf * Ntot * dens * SQR(SLaver) * SQR(Rcut) * __sinf(qrcut) / (lq * (SQR(qrcut) - SQR(PIf)));
			else I[iq] += 4.f * PIf * Ntot * dens * SQR(SLaver) * (Rcut * __cosf(qrcut) - __sinf(qrcut) / lq) / SQR(lq);
		}
	}
}

//Adds the average density correction to the scattering intensity when the cut-off is enabled (cfg.cutoff == true)
void AddCutoffCUDA(const unsigned int GSadd, float * const dI, const unsigned int *const NatomEl, const config * const cfg, const float * const dFF, const vector<double> SL, const float * const dq, const unsigned int Ntot) {
	if (cfg->source == xray) {
		unsigned int * dNatomEl = NULL;
		cudaMalloc(&dNatomEl, cfg->Nel * sizeof(unsigned int));
		cudaMemcpy(dNatomEl, NatomEl, cfg->Nel * sizeof(unsigned int), cudaMemcpyHostToDevice);
		boinc_begin_critical_section();
		AddCutoffKernelXray << <GSadd, BlockSize1Dsmall >> >(dI, dq, dFF, dNatomEl, cfg->Nel, Ntot, cfg->q.N, float(cfg->Rcutoff), float(cfg->p0), cfg->damping);
		cudaThreadSynchronize();
		boinc_end_critical_section();
		cudaFree(dNatomEl);
	}
	else {
		float SLav = 0;
		for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++) SLav += float(SL[iEl]) * NatomEl[iEl];
		SLav /= Ntot;
		boinc_begin_critical_section();
		AddCutoffKernelNeutron << <GSadd, BlockSize1Dsmall >> >(dI, dq, SLav, Ntot, cfg->q.N, float(cfg->Rcutoff), float(cfg->p0), cfg->damping);
		cudaThreadSynchronize();
		boinc_end_critical_section();
	}
}

//Organazies the computations of the scattering intensity (powder diffraction pattern) using the histogram of interatomic distances with CUDA
void calcInt1DHistCuda(const int DeviceNUM, double ** const I, const unsigned long long int * const rij_hist, const unsigned int *const NatomEl, const config * const cfg, const float * const dFF, const vector<double> SL, const float * const dq, const unsigned int Ntot){
	cudaDeviceProp deviceProp;
	cudaGetDeviceProperties(&deviceProp, DeviceNUM);//getting device properties
	const int cc = deviceProp.major * 10 + deviceProp.minor;//device compute capability
	unsigned int BlockSize = BlockSize1Dlarge;//setting the size of the thread blocks to 1024 (default)	
	if (cc < 30) BlockSize = BlockSize1Dmedium;//setting the size of the thread blocks to 512 for the devices with CC < 3.0
	const unsigned int GridSize = MIN(256, cfg->Nhist / BlockSize + BOOL(cfg->Nhist % BlockSize));
	const unsigned int GFLOPS = GetGFLOPS(deviceProp);//theoretical peak GPU performance
	unsigned int MaxBinsPerBlock = cfg->Nhist / GridSize + BOOL(cfg->Nhist % GridSize);
	if (deviceProp.kernelExecTimeoutEnabled)	{//killswitch is enabled, so the time limit should not be exceeded
		const double tmax = 0.02; //maximum kernel time execution in seconds
		const double k = 1.5e-5; // t = k * Nq * MaxBinsPerBlock / GFLOPS
		MaxBinsPerBlock = MIN((unsigned int)(tmax * GFLOPS / (k * cfg->q.N)), MaxBinsPerBlock);
	}
	float *dI = NULL;//device array for scattering intensity
	const unsigned int Isize = GridSize * cfg->q.N;//each block writes to it's own copy of scattering intensity array
	cudaMalloc(&dI, Isize * sizeof(float));//allocating the device memory for the scattering intensity array
	const unsigned int GSzero = MIN(65535, Isize / BlockSize + BOOL(Isize % BlockSize));//grid size for zero1DFloatArrayKernel
	boinc_begin_critical_section();
	for (unsigned int iter = 0; iter < Isize / BlockSize + BOOL(Isize % BlockSize); iter += GSzero) zero1DFloatArrayKernel << <GSzero, BlockSize >> >(dI + iter*BlockSize, Isize - iter*BlockSize);//reseting intensity array
	//cudaThreadSynchronize();//synchronizing before calculation starts
	boinc_end_critical_section();
	const unsigned int GSadd = cfg->q.N / BlockSize1Dsmall + BOOL(cfg->q.N % BlockSize1Dsmall);//grid size for addIKernelXray/addIKernelNeutron
	unsigned int Nstart = 0;
	for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++) {
		if (cfg->source == xray) addIKernelXray << <GSadd, BlockSize1Dsmall >> > (dI, dFF + iEl * cfg->q.N, cfg->q.N, NatomEl[iEl]);//add contribution form diagonal (i==j) elements in Debye sum
		else addIKernelNeutron << <GSadd, BlockSize1Dsmall >> > (dI, cfg->q.N, float(SQR(SL[iEl]) * NatomEl[iEl]));
		//cudaThreadSynchronize();//synchronizing before main calculation starts
		for (unsigned int jEl = iEl; jEl < cfg->Nel; jEl++, Nstart += cfg->Nhist){
			for (unsigned int iBin = 0; iBin < cfg->Nhist; iBin += GridSize * MaxBinsPerBlock) {//iterations to avoid killswitch triggering
				/*float time;
				cudaEvent_t start, stop;
				cudaEventCreate(&start);
				cudaEventCreate(&stop);
				cudaEventRecord(start, 0);*/
				boinc_begin_critical_section();
				if (cfg->source == xray) {//Xray
					if (cc >= 30) calcIntHistKernel <BlockSize1Dlarge> << <GridSize, BlockSize >> > (xray, dI, dFF + iEl * cfg->q.N, dFF + jEl * cfg->q.N, 0, dq, cfg->q.N, rij_hist + Nstart, iBin, cfg->Nhist, MaxBinsPerBlock, float(cfg->hist_bin), float(cfg->Rcutoff), cfg->damping);
					else calcIntHistKernel <BlockSize1Dmedium> << <GridSize, BlockSize >> > (xray, dI, dFF + iEl * cfg->q.N, dFF + jEl * cfg->q.N, 0, dq, cfg->q.N, rij_hist + Nstart, iBin, cfg->Nhist, MaxBinsPerBlock, float(cfg->hist_bin), float(cfg->Rcutoff), cfg->damping);
				}
				else {//neutron
					if (cc >= 30) calcIntHistKernel <BlockSize1Dlarge> << <GridSize, BlockSize >> > (neutron, dI, NULL, NULL, float(SL[iEl] * SL[jEl]), dq, cfg->q.N, rij_hist + Nstart, iBin, cfg->Nhist, MaxBinsPerBlock, float(cfg->hist_bin), float(cfg->Rcutoff), cfg->damping);
					else calcIntHistKernel <BlockSize1Dmedium> << <GridSize, BlockSize >> > (neutron, dI, NULL, NULL, float(SL[iEl] * SL[jEl]), dq, cfg->q.N, rij_hist + Nstart, iBin, cfg->Nhist, MaxBinsPerBlock, float(cfg->hist_bin), float(cfg->Rcutoff), cfg->damping);
				}
				/*cudaEventRecord(stop, 0);
				cudaEventSynchronize(stop);
				cudaEventElapsedTime(&time, start, stop);
				cout << "calcIntHistKernel execution time is: " << time << " ms\n" << endl;*/
				if (cfg->wait4kernel) cudaThreadSynchronize();//synchronizing before the next iteration step
				boinc_end_critical_section();
			}
		}
	}
	boinc_begin_critical_section();
	sumIKernel << <GSadd, BlockSize1Dsmall >> >(dI, cfg->q.N, GridSize);//summing intensity copies
	//cudaThreadSynchronize();//synchronizing threads before multiplying the intensity by a polarization factor
	boinc_end_critical_section();
	if (cfg->cutoff) AddCutoffCUDA(GSadd, dI, NatomEl, cfg, dFF, SL, dq, Ntot);
	if (cfg->PolarFactor) PolarFactor1DKernel << <GSadd, BlockSize1Dsmall >> >(dI, cfg->q.N, dq, float(cfg->lambda));
	float * const hI = new float[cfg->q.N];
	cudaMemcpy(hI, dI, cfg->q.N * sizeof(float), cudaMemcpyDeviceToHost);//copying intensity array from the device to the host
	cudaFree(dI);//deallocating memory for intensity array
	*I = new double[cfg->q.N];
	for (unsigned int iq = 0; iq < cfg->q.N; iq++) (*I)[iq] = double(hI[iq]) / Ntot;//normalizing
	delete[] hI;
}

//Depending on the computational scenario organazies the computations of the scattering intensity (powder diffraction pattern) or PDF using the histogram of interatomic distances with CUDA
void calcHistandPatternCuda(const int DeviceNUM, double ** const I, const config * const cfg, const unsigned int * const NatomEl, const unsigned int * const NatomEl_outer, const float4 * const ra, const float * const dFF, const vector<double> SL, const float * const dq, ostringstream *boinc_msg) {
	float time;
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start, 0);
	unsigned long long int *rij_hist = NULL;//array for pair-distribution histogram (device only)
	calcHistCuda(DeviceNUM, &rij_hist, ra, NatomEl, NatomEl_outer, cfg, boinc_msg);//calculating the histogram
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time, start, stop);
	cout << "Histogram calculation time: " << time / 1000 << " s" << endl;
	*boinc_msg << "Histogram calculation time: " << time / 1000 << " s\n";
	unsigned int Ntot = 0;
	for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++) Ntot += NatomEl[iEl];//calculating the total number of atoms
	cudaEventRecord(start, 0);
	calcInt1DHistCuda(DeviceNUM, I, rij_hist, NatomEl, cfg, dFF, SL, dq, Ntot);//calculating the scattering intensity using the pair-distribution histogram
	cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time, start, stop);
	cout << "1D pattern calculation time: " << time / 1000 << " s" << endl;
	*boinc_msg << "1D pattern calculation time: " << time / 1000 << " s\n";
	if (rij_hist != NULL) cudaFree(rij_hist);//deallocating memory for pair distribution histogram
}


//Queries all CUDA devices. Checks and sets the CUDA device number
//Returns 0 if OK and - 1 if no CUDA devices found
int SetDeviceCuda(const int DeviceNUM, ostringstream *boinc_msg){
	int nDevices;
	cudaGetDeviceCount(&nDevices);
	if (!nDevices) {
		cout << "Error: No CUDA devices found." << endl;
		*boinc_msg << "Error: No CUDA devices found.\n";
		return -1;
	}
	if (DeviceNUM >= nDevices){
		cout << "Error: Unable to set CUDA device " << DeviceNUM << ". The total number of CUDA devices is " << nDevices << ".\n";
		*boinc_msg << "Error: Unable to set CUDA device " << DeviceNUM << ". The total number of CUDA devices is " << nDevices << ".\n";
		return -1;
	}
	cudaSetDevice(DeviceNUM);
	cudaSetDeviceFlags(cudaDeviceBlockingSync);
	cudaDeviceProp deviceProp;
	cudaGetDeviceProperties(&deviceProp, DeviceNUM);
	cout << "Selected CUDA device #" << DeviceNUM << " out of " << nDevices << ":" << endl;
	*boinc_msg << "Selected CUDA device #" << DeviceNUM << " out of " << nDevices << ":\n";
	GetGFLOPS(deviceProp, true, boinc_msg);
	return 0;
	
}

//Copies the atomic coordinates (ra), scattering vector magnitude (q) and the x-ray atomic form-factors (FF) to the device memory	
void dataCopyCUDA(const double *const q, const config * const cfg, const vector < vect3d <float> > * const ra, float4 ** const dra, float ** const dFF, float ** const dq, const vector <double*> FF){
	//copying the main data to the device memory
	float * const qfloat = new float[cfg->q.N]; // temporary float array for the scattering vector magnitude
	for (unsigned int iq = 0; iq < cfg->q.N; iq++) qfloat[iq] = (float)q[iq];//converting scattering vector magnitude from double to float
	cudaMalloc(dq, cfg->q.N * sizeof(float));//allocating memory for the scattering vector magnitude array
	cudaMemcpy(*dq, qfloat, cfg->q.N * sizeof(float), cudaMemcpyHostToDevice);//copying scattering vector magnitude array from the host to the device
	delete[] qfloat;//deleting temporary array
	if (cfg->source == xray) {
		cudaMalloc(dFF, cfg->q.N * cfg->Nel * sizeof(float));//allocating device memory for the atomic form-factors
		float * const FFfloat = new float[cfg->q.N * cfg->Nel];
		for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++){
			for (unsigned int iq = 0; iq < cfg->q.N; iq++) FFfloat[iEl * cfg->q.N + iq] = float(FF[iEl][iq]);//converting form-factors from double to float				
		}
		cudaMemcpy(*dFF, FFfloat, cfg->Nel * cfg->q.N * sizeof(float), cudaMemcpyHostToDevice);//copying form-factors from the host to the device
		delete[] FFfloat;//deleting temporary array
	}
	unsigned int Nat = 0;
	for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++) Nat += (unsigned int)ra[iEl].size();
	cudaMalloc(dra, Nat * sizeof(float4));//allocating device memory for the atomic coordinates array
	float4 * const hra = new float4[Nat]; //temporary host array for atomic coordinates
	unsigned int iAtom = 0;
	for (unsigned int iEl = 0; iEl < cfg->Nel; iEl++){
		for (vector<vect3d <float> >::const_iterator ri = ra[iEl].begin(); ri != ra[iEl].end(); ri++, iAtom++){
			hra[iAtom] = make_float4((float)ri->x, (float)ri->y, (float)ri->z, 0);//converting atomic coordinates from vect3d <double> to float4
		}
	}	
	cudaMemcpy(*dra, hra, Nat * sizeof(float4), cudaMemcpyHostToDevice);//copying atomic coordinates from the host to the device
	delete[] hra;//deleting temporary array
}

//Deletes the atomic coordinates (ra), scattering vector magnitude (dq) and the x-ray atomic form-factors (dFF) from the device memory
void delDataFromDevice(float4 * const ra, float * const dFF,float * const dq, const unsigned int Nel){
	cudaFree(ra);//deallocating device memory for the atomic coordinates array
	if (dq != NULL) cudaFree(dq);//deallocating memory for the scattering vector magnitude array
	if (dFF != NULL) cudaFree(dFF);//deallocating device memory for the atomic form-factors
	cudaDeviceReset();//NVIDIA Profiler works improperly without this
}
#endif
